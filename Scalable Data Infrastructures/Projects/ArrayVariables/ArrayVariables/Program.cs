﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArrayVariables
{
    class Program
    {
        static void Main(string[] args)
        {

            //Arrays Are Signified By []
            //datatype arrayName = new datatype[Size of Array];

            //Create / Declare The Array
            int[] bills = new int[3];

            //Fill / Define The Array
            //arrayName[Index#] = value;
            //Index Numbers Always Start With 0
            //Index Number Of The Last Item Is One Less Than The Size Of The Array

            bills[0] = 130;
            bills[1] = 150;
            bills[2] = 2000;

            //Use The Array Items
            Console.WriteLine("The Water Bill For This Month Is $" + bills[1]);
            Console.WriteLine("The Electric Bill For This Month Is $" + bills[0]);
            Console.WriteLine("The Rent For This Month Is $" + bills[2]);

            //Let's Do Math With Arrays
            //Total Of Bills
            int totalBills = bills[0] + bills[1] + bills[2];
            Console.WriteLine("The Total Bills For The Month Is $" + totalBills);


            //Declare AND Define Array
            //datatype[] arrayName = new string[]{value1,value2,value3, etc...};
            string[] fruits = new string[] {"Apple","Pear","Banana","Grapes"};
            Console.WriteLine(fruits[0]);

            //To Find Out How Big An Array Is
            //Length Property
            //Dot Syntax - Use A Period
            Console.WriteLine(fruits.Length);

            //Change Any Value Inside An Array
            //Stays Changed Going Forward Until It Is Changed Again
            Console.WriteLine(fruits[0]);
            //variableName = New Value
            fruits[0] = "Kiwi";
            Console.WriteLine(fruits[0]);

        }
    }
}
