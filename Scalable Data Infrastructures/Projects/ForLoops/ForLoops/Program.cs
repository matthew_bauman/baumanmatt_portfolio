﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ForLoops
{
    class Program
    {
        static void Main(string[] args)
        {

            // For Loops
            // for(variable; test; increment) {What to do}
            for(int counter = 0; counter <= 11; counter++)
                {
                    Console.WriteLine("The value of counter is {0}", counter);

                    // If counter gets to 7, then break:
                    if (counter == 7)
                        {
                            Console.WriteLine("Stopping the loop");
                            break; 
                        }
                }

            // Break will stop the loop totally. Continue will only skip the step (iteration).
            // Create a new loop:
            for(int i = 0; i < 50; i++)
                {
                    // Skip every other number:

                    if(i % 2 == 0)
                        {
                            // Is the number even?
                            // This will skip all even numbers
                            continue; 
                        }
                    Console.WriteLine("The value of i is {0}.", i);
                }
           


            // For Each Loops
            // Create an array to loop through. 
            int[] myBills = new int[4] { 30, 40, 50, 60 };

            // Create a variable to hold the sum:
            int totalSum = 0;

            // Create the foreach loop:
            foreach(int arrayItem in myBills)
                {
                    // Each item in the array will loop:
                    Console.WriteLine("The item in the array is {0}", arrayItem);   
                }

            foreach(int eachBill in myBills)
                {
                    totalSum = totalSum + eachBill;
                    // totalSum += eachBill;

                    
                }

            Console.WriteLine("The sum of our bills is {0}", totalSum);




        }
    }
}
