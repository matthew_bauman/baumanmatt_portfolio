﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TotalProject
{
    class Program
    {
        static void Main(string[] args)
        {

            /*
                Name: Matthew Bauman
                Date: 04/14/2017
                Section 02
                Total Project
            */

            // Calcuate the area and the perimeter of a rectangle from user prompts:

            // Tell the user what we are doing and then ask for the width:
            Console.Write("Hello! We are going to find the area and perimeter of a rectangle.\r\nPlease type in a value for the width and press enter: ");

            // Capture the users response:
            string widthString = Console.ReadLine();

            // Declare a variable to hold the converted value:
            double widthNum;

            // Validate the user is typing in a valid number with WHILE loop:
            while(!double.TryParse(widthString, out widthNum) || widthNum < 0)
                {

                    // Alert the user to the error:
                    Console.Write("Please only type in numbers with no blanks!\r\nWhat is the width of the rectangle? ");

                    // Re-capture the user's response in the SAME variable as before:
                    widthString = Console.ReadLine();    

                }

            // Tell the user the width, say thanks, and then ask for a length:
            Console.Write("Got It! You have entered a width of {0}. What is the length? ", widthNum);

            // Capture the user's response:
            string lengthString = Console.ReadLine();

            // Declare a variable to hold the converted variable:
            double lengthNum;

            // Validate the user is typing in a valid number with WHILE loop:
            while (!double.TryParse(lengthString, out lengthNum) || lengthNum < 0)
                {

                    // Alert the user to the error:
                    Console.Write("Please only type in numbers with no blanks!\r\nWhat is the length of the rectangle? ");

                    // Re-capture the user's response in the SAME variable as before:
                    lengthString = Console.ReadLine();

                }

            // Tell the user we got the length and tell them the next step:
            Console.WriteLine("Thank you! You entered a width of {0} and a length of {1}.\r\nWe will now calculate the perimeter and area.\r\n", widthNum, lengthNum);

            // Go create a function to calculate the perimeter.

            // Function call the CalcPerimeter method / function:
            // Remember to CATCH the returned value with a variable AND use Arguments:
            double perimeter = CalcPerimeter(widthNum, lengthNum);

            // Report to the User:
            Console.WriteLine("The perimeter of the rectangle is {0}.\r\n", perimeter);

            // Create a function to calculate the area of the rectangle.

            // Function call and store the returned value:
            double area = CalcArea(widthNum, lengthNum);

            // Report to the user:
            Console.WriteLine("The area of the rectangle is {0}.", area);

            // Tell the user we want to find the volume and ask for height:
            Console.Write("Let's find the volume if the rectangle was actually a rectangular prism.\r\nWhat is the height: ");

            // Create a variable to hold the height:
            string heightString = Console.ReadLine();

            // Declare a variable to hold converted height:
            double heightNum;

            // Validate the user is typing in a valid number with WHILE loop:
            while (!double.TryParse(heightString, out heightNum) || heightNum < 0)
                {

                    // Alert the user to the error:
                    Console.Write("Please only type in numbers with no blanks!\r\nWhat is the height of the rectangle? ");

                    // Re-capture the user's response in the SAME variable as before:
                    heightString = Console.ReadLine();

                }

            // Tell the user:
            Console.WriteLine("You typed in a height of {0}. We will now find the volume!\r\n", heightNum);

            // Function call and save the returned value:
            double volume = CalcVolume(widthNum, lengthNum, heightNum);

            // Final output to the user:
            Console.WriteLine("The volume of the rectangular prism is {0}.", volume);


            /*
             width = 4
             length = 5
             height = 6
             
             Computer calculated these values:
             Perimeter = 18
             Area = 20
             Volume = 120
             All values are correct and checked with a calculator.
            

        }

        public static double CalcPerimeter(double wid, double len)
        {

            // Create a variable for perimeter and do the math:
            double peri = 2 * wid + 2 * len;
            return peri;

        }

        public static double CalcArea(double w, double l)
        {

            // Do the math and store in a variable and return to main method:
            double area = w * l;
            return area;

        }

        public static double CalcVolume(double w, double l, double h)
        {

            // Calculate the volume and return to the main method:
            double volume = w * l * h;
            return volume;

        }

    }
}
