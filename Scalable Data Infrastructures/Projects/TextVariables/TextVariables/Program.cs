﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TextVariables
{
    class Program
    {
        static void Main(string[] args)
        {

            //Variables - Characters

            //Characters - "char" Is Only The Size Of ONE Character
            //Must Be Surrounded By Single Quotes ''

            //Declare A Variable
            char firstLetter;

            //Define A Variable
            firstLetter = 'A';

            //Print The Variable To Our Console
            Console.WriteLine(firstLetter);

            //Declare AND Define A Variable In One Step
            char secondCharacter = 'b';

            //Print The New Variable
            Console.WriteLine(secondCharacter);

            //String Of Characters - "string" Basically Anything Bigger Than A Single Character
            //Must Be Surrounded By Double Quotes ""

            string wholeSentence = "This is an example of a sentence.";

            //Print Out The String Variable
            Console.WriteLine(wholeSentence);

            //Combine Strings Together
            //Use The Plus Sign - Concatenation

            string combinedString = "First Part " + "Second Part";
            Console.WriteLine(combinedString);

            //Spaces?
            string firstName = "Kermit";
            string lastName = "The Frog";
            string wholeName = firstName + " " + lastName;
            Console.WriteLine(wholeName);
        
        }
    }
}
