﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Matthew_Bauman_LogicLoops
{
    class Program
    {
        static void Main(string[] args)
        {
            
            //  NAME:  Matthew Bauman
            //  DATE:  04/09/2017
            //  Scalable Data Infrastructures Section 02
            //  Logic & Loops Assignment 

            // Problem #1 - Tire Pressure

            Console.WriteLine("Problem #1 - Tire Pressure");
            Console.WriteLine("Let's ensure that your tires meet maintenance standards.");

            // Prompting the user for the LEFT FRONT tire pressure:
            Console.Write("Please enter the tire pressure of the LEFT FRONT tire: ");

            // Listen for user input and create variable to store converted string (LEFT FRONT):
            string leftFrontString = Console.ReadLine();
            int leftFrontNum;

            // Confirming user did not leave input blank, entered a number not a string and that value is positive:
            while (string.IsNullOrWhiteSpace(leftFrontString) || !int.TryParse(leftFrontString, out leftFrontNum) || leftFrontNum < 0)
                {
                    // Inform user that their input is blank or contains letters or is not in correct range:
                    Console.Write("You have entered something other than a positive number, please try again: ");

                    // Re-Listen for user input:
                    leftFrontString = Console.ReadLine();
                }

            // Prompting the user for the RIGHT FRONT tire pressure:
            Console.Write("Please enter the tire pressure of the RIGHT FRONT tire: ");

            // Listen for user input and create variable to store converted string (RIGHT FRONT):
            string rightFrontString = Console.ReadLine();
            int rightFrontNum;

            // Confirming user did not leave input blank, entered a number not a string and that value is positive:
            while (string.IsNullOrWhiteSpace(rightFrontString) || !int.TryParse(rightFrontString, out rightFrontNum) || rightFrontNum < 0)
                {
                    // Inform user that their input is blank or contains letters or is not in correct range:
                    Console.Write("You have entered something other than a positive number, please try again: ");

                    // Re-Listen for user input:
                    rightFrontString = Console.ReadLine();
                }

            // Prompting the user for the LEFT REAR tire pressure:
            Console.Write("Please enter the tire pressure of the LEFT REAR tire: ");

            // Listen for user input and create variable to store converted string (LEFT REAR):
            string leftRearString = Console.ReadLine();
            int leftRearNum;

            // Confirming user did not leave input blank, entered a number not a string and that value is positive:
            while (string.IsNullOrWhiteSpace(leftRearString) || !int.TryParse(leftRearString, out leftRearNum) || leftRearNum < 0)
                {
                    // Inform user that their input is blank or contains letters or is not in correct range:
                    Console.Write("You have entered something other than a positive number, please try again: ");

                    // Re-Listen for user input:
                    leftRearString = Console.ReadLine();
                }

            // Prompting the user for the RIGHT REAR tire pressure:
            Console.Write("Please enter the tire pressure of the RIGHT REAR tire: ");

            // Listen for user input and create variable to store converted string (RIGHT REAR):
            string rightRearString = Console.ReadLine();
            int rightRearNum;

            // Confirming user did not leave input blank, entered a number not a string and that value is positive:
            while (string.IsNullOrWhiteSpace(rightRearString) || !int.TryParse(rightRearString, out rightRearNum) || rightRearNum < 0)
                {
                    // Inform user that their input is blank or contains letters or is not in correct range:
                    Console.Write("You have entered something other than a positive number, please try again: ");

                    // Re-Listen for user input:
                    rightRearString = Console.ReadLine();
                }

            // Declaring AND defining an array to store the tire pressures.
            int[] tireArray = new int[4] { leftFrontNum, rightFrontNum, leftRearNum, rightRearNum };

            // Determining if the tires DO or DO NOT meet standards and outputting to the user:
            if ((tireArray[0] == tireArray[1]) && (tireArray[2] == tireArray[3]))
                {
                    Console.WriteLine("Tires pass spec!");
                }
            else
                {
                    Console.WriteLine("Get your tires checked out!");
                }

            /*
                Data Sets to Test:

                Problem #1 - Tire Pressure
                Let's ensure that your tires meet maintenance standards.
                Please enter the tire pressure of the LEFT FRONT tire: 32
                Please enter the tire pressure of the RIGHT FRONT tire: 32
                Please enter the tire pressure of the LEFT REAR tire: 30
                Please enter the tire pressure of the RIGHT REAR tire: 30
                Tires pass spec!
                Press any key to continue . . .

                Problem #1 - Tire Pressure
                Let's ensure that your tires meet maintenance standards.
                Please enter the tire pressure of the LEFT FRONT tire: 36
                Please enter the tire pressure of the RIGHT FRONT tire: 32
                Please enter the tire pressure of the LEFT REAR tire: 25
                Please enter the tire pressure of the RIGHT REAR tire: 25
                Get your tires checked out!
                Press any key to continue . . .

                Problem #1 - Tire Pressure
                Let's ensure that your tires meet maintenance standards.
                Please enter the tire pressure of the LEFT FRONT tire:
                You have entered something other than a positive number, please try again:
                You have entered something other than a positive number, please try again: a
                You have entered something other than a positive number, please try again: 1-
                You have entered something other than a positive number, please try again: 32
                Please enter the tire pressure of the RIGHT FRONT tire:
                You have entered something other than a positive number, please try again: b
                You have entered something other than a positive number, please try again: -2
                You have entered something other than a positive number, please try again: 32
                Please enter the tire pressure of the LEFT REAR tire:
                You have entered something other than a positive number, please try again: e
                You have entered something other than a positive number, please try again: -50
                You have entered something other than a positive number, please try again: 15
                Please enter the tire pressure of the RIGHT REAR tire:
                You have entered something other than a positive number, please try again: string
                You have entered something other than a positive number, please try again: -43
                You have entered something other than a positive number, please try again: 15
                Tires pass spec!
                Press any key to continue . . .
            */

            // Problem #2 Movie Ticket Price
            Console.WriteLine("\r\nProblem #2 - Movie Ticket Price");
            Console.WriteLine("Let's find out what the cost of your movie ticket will be!");

            // Prompting user for their age:
            Console.Write("Please enter your age: ");

            // Listening for user age input and creating variable to store converted string:
            string userAgeString = Console.ReadLine();
            int userAgeNum;


            // Confirming user did not leave input blank, entered a number not a string and that value is positive:
            while (string.IsNullOrWhiteSpace(userAgeString) || !int.TryParse(userAgeString, out userAgeNum) || userAgeNum < 0)
                {
                    // Inform user that their input is blank or contains letters or is not in correct range:
                    Console.Write("You have entered something other than a positive number, please try again: ");

                    // Re-Listen for user input:
                    userAgeString = Console.ReadLine();
                }

            // Prompting user for the time of the movie (In Military Time):
            Console.Write("Please enter the movie showtime (In military time): ");

            // Listening for user age input and creating variable to store converted string:
            string userTimeString = Console.ReadLine();
            int userTimeNum;


            // Confirming user did not leave input blank, entered a number not a string and that value is positive:
            while (string.IsNullOrWhiteSpace(userTimeString) || !int.TryParse(userTimeString, out userTimeNum) || userTimeNum < 0)
                {
                    // Inform user that their input is blank or contains letters or is not in correct range:
                    Console.Write("You have entered something other than a positive number, please try again: ");

                    // Re-Listen for user input:
                    userAgeString = Console.ReadLine();
                }

            // Determining what the user's ticket price is and outputting it to them:
            // Assignment sheet says "If you are a senior (55 and older) or you are UNDER 10 not "10 AND under", so "10" not between 2 and 5pm will result in a $12.00 price.
            if(userAgeNum >= 55 || userAgeNum < 10 || (userTimeNum >= 14 && userTimeNum <= 17))
                {
                    Console.WriteLine("The ticket price is: $7.00");
                }
            else
                {
                    Console.WriteLine("The ticket price is: $12.00");
                }

            /*
            Datasets to Test:

            Let's find out what the cost of your movie ticket will be!
            Please enter your age: 57
            Please enter the movie showtime (In military time): 20
            The ticket price is: $7.00  
             
            Let's find out what the cost of your movie ticket will be!
            Please enter your age: 9
            Please enter the movie showtime (In military time): 20
            The ticket price is: $7.00  
            
            Let's find out what the cost of your movie ticket will be!
            Please enter your age: 38
            Please enter the movie showtime (In military time): 20
            The ticket price is: $12.00       

            Let's find out what the cost of your movie ticket will be!
            Please enter your age: 25
            Please enter the movie showtime (In military time): 16
            The ticket price is: $7.00

            Let's find out what the cost of your movie ticket will be!
            Please enter your age: 10
            Please enter the movie showtime (In military time): 12
            The ticket price is: $12.00
            
            */


            // Problem #3 - For Loop: Add Up The Odds or Evens
            Console.WriteLine("\r\nProblem #3 - For Loop: Add Up The Odds or Evens");
            Console.WriteLine("Let's find the sum of either the ODD or EVEN numbers in an array:");

            // Creating the given array of INTEGERS:
            int[] arrayOne = new int[7] { 1, 2, 3, 4, 5, 6, 7 };
            int[] arrayTwo = new int[6] { 12, 13, 14, 15, 16, 17 }; 
            int[] arrayCustom = new int[10] { 1, 1, 2, 3, 5, 8, 13, 21, 34, 55 };

            // Creating a Sum variable:
            int arraySum = 0;

            // Prompting user for even or odd:
            Console.Write("Would you like to see the sum of the EVEN or ODD numbers? ");

            // Listening for user input:
            string userInput = Console.ReadLine();
            userInput = userInput.ToUpper();

            // Defining Comparison Strings
            string isEven = "EVEN";
            string isOdd = "ODD";

            // Confirming the user entered SOMETHING and that they entered either "Even" or "Odd" regardless of the case used:
            while (string.IsNullOrWhiteSpace(userInput) || !(userInput == isEven || userInput == isOdd))
                {
                    // Inform user that their input is blank / is not the word "EVEN" or "ODD" (regardless of case): 
                    Console.Write("You have entered something other than EVEN or ODD, please try again: ");

                    // Re-Listen for user input:
                    userInput = Console.ReadLine();
                    userInput = userInput.ToUpper();
                }

          
            // Creating the FOR loop to add up array indices based on user input and outputting to console: 
            if(userInput == isEven)
                {
                    foreach(int eachNum in arrayCustom)
                        {
                            if(eachNum % 2 == 0)
                                {
                                    arraySum += eachNum;
                                }
                        }

                        Console.WriteLine("The even numbers add up to {0}", arraySum);
    
                }
            else if(userInput == isOdd)
                {
                    foreach(int eachNum in arrayCustom)
                        {
                            if(eachNum % 2 == 1)
                                {
                                    arraySum += eachNum;
                                }
                        }
                        Console.WriteLine("The odd numbers add up to {0}", arraySum);
                }

            /*
            Data Sets To Test:

            Array One:
            Let's find the sum of either the ODD or EVEN numbers in an array:
            Would you like to see the sum of the EVEN or ODD numbers? even
            The even numbers add up to 12

            Let's find the sum of either the ODD or EVEN numbers in an array:
            Would you like to see the sum of the EVEN or ODD numbers? odd
            The odd numbers add up to 16

            ArrayTwo:
            Let's find the sum of either the ODD or EVEN numbers in an array:
            Would you like to see the sum of the EVEN or ODD numbers? even
            The even numbers add up to 42

            Let's find the sum of either the ODD or EVEN numbers in an array:
            Would you like to see the sum of the EVEN or ODD numbers? odd
            The odd numbers add up to 45

            My Array:
            Let's find the sum of either the ODD or EVEN numbers in an array:
            Would you like to see the sum of the EVEN or ODD numbers?
            You have entered something other than EVEN or ODD, please try again: 1
            You have entered something other than EVEN or ODD, please try again: even
            The even numbers add up to 44

            Let's find the sum of either the ODD or EVEN numbers in an array:
            Would you like to see the sum of the EVEN or ODD numbers?
            You have entered something other than EVEN or ODD, please try again: 1
            You have entered something other than EVEN or ODD, please try again: OdD
            The odd numbers add up to 99


            */


            // Problem #4 - While Loop: Charge It!
            Console.WriteLine("\r\nProblem #4 - While Loop: Charge It!");

            // Prompting user for their credit limit:
            Console.Write("Please enter your credit limit: $");

            // Listening for user's CREDIT LIMIT and creating a variable to store the number value:
            string limitString = Console.ReadLine();
            decimal limitNum;
            

            // Confirming user did not leave input blank, entered a number not a string and that value is positive:
            while (string.IsNullOrWhiteSpace(limitString) || !decimal.TryParse(limitString, out limitNum) || limitNum < 0)
                {
                    // Inform user that their input is blank, contains letters or is not in correct range:
                    Console.Write("You have entered something other than a positive number, please try again: ");

                    // Re-Listen for user input:
                    limitString = Console.ReadLine();
                }


            // Creating purchase sum variable as well as a variable to keep track of how much credit is left:
            decimal purchaseSum = 0.00m;
            decimal creditLeft = limitNum;
           

            // Loop to add user purchase totals together until credit limit has been reached / exceeded:

            do
                {
                    // Ask user for the cost of their purchase:
                    Console.Write("Please enter the cost of your purchase: $");

                    // Listening for cost of user's PURCHASE and creating variable to store the number value:
                    string purchaseString = Console.ReadLine();
                    decimal purchaseNum;
                    

                    // Confirming user did not leave input blank, entered a number not a string and that value is positive:
                    while (string.IsNullOrWhiteSpace(purchaseString) || !decimal.TryParse(purchaseString, out purchaseNum) || purchaseNum < 0)
                        {
                            // Inform user that their input is blank, contains letters or is not in correct range:
                            Console.Write("You have entered something other than a positive number, please try again: $");

                            // Re-Listen for user input:
                            purchaseString = Console.ReadLine();
                        }

                    
                    purchaseSum += purchaseNum;
                    creditLeft -= purchaseNum;
                    string formattedPurchaseNum = String.Format("{0:0.00}", purchaseNum);
                    string formattedPurchaseSum = String.Format("{0:0.00}", purchaseSum);
                    string formattedCreditLeft = String.Format("{0:0.00}", creditLeft);

                    Console.WriteLine("With your current purchase of ${0}, you can still spend ${1}.", formattedPurchaseNum, formattedCreditLeft);

            } while (purchaseSum < limitNum);

            // Declaring and defining variable to determine how much the user exceeded their limit by:
            decimal overCredit = purchaseSum - limitNum;
            string formattedOverCredit= String.Format("{0:0.00}", overCredit);


            // Informing the user that they have exceeded their credit limit:
            Console.WriteLine("With your last purchase you have reached your credit limit and exceeded it by ${0}.", formattedOverCredit);

            /*
            Data Sets To Test:

            Please enter your credit limit: $20
            Please enter the cost of your purchase: $5
            With your current purchase of $5.00, you can still spend $15.00.
            Please enter the cost of your purchase: $12
            With your current purchase of $12.00, you can still spend $3.00.
            Please enter the cost of your purchase: $7
            With your current purchase of $7.00, you can still spend $-4.00.
            With your last purchase you have reached your credit limit and exceeded it by $4.00.

            Please enter your credit limit: $50
            Please enter the cost of your purchase: $25.99
            With your current purchase of $25.99, you can still spend $24.01.
            Please enter the cost of your purchase: $19.99
            With your current purchase of $19.99, you can still spend $4.02.
            Please enter the cost of your purchase: $3.02
            With your current purchase of $3.02, you can still spend $1.00.
            Please enter the cost of your purchase: $.85
            With your current purchase of $0.85, you can still spend $0.15.
            Please enter the cost of your purchase: $20
            With your current purchase of $20.00, you can still spend $-19.85.
            With your last purchase you have reached your credit limit and exceeded it by $19.85.


            */
        }
    }
}
