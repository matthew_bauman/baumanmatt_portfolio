﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BasicFunctions
{
    class Program
    {
        static void Main(string[] args)
        {

            Console.WriteLine("Before the function call.");

            // Create a function call that will start our PrintMore() function:
            // Put the name of the function and ();
            PrintMore();

            Console.WriteLine("After the function call.");

            // A second function call:
            PrintMore();

            // A call to the second new function PrintEnjoy();:
            PrintEnjoy();

            string userName = "Bob";
            Console.WriteLine(userName);

        }

        public static void PrintEnjoy()
        {

            Console.WriteLine("We hope you enjoy the program. Now go away!");

        }

        public static void PrintMore()
        {

            // Code that will run when I "Call" this method / function:
            string userName = "Matt";
            Console.WriteLine("Welcome {0} to the program!", userName);

        }


    }
}
