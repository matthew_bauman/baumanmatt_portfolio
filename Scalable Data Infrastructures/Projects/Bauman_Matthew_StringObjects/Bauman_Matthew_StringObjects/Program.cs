﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bauman_Matthew_StringObjects
{
    class Program
    {
        static void Main(string[] args)
        {

            /*
            Name: Matthew Bauman
            Date: 04/15/2017
            SDI Section 02
            String Objects Assignment
            */

            // Problem #1 - Email Address Checker
            Console.WriteLine("Problem #1 - Email Address Checker\r\nLet's verify if your entered email address is a proper address!");

            // Prompt the user for an email address to verify:
            Console.Write("Please enter an email address to be verified: ");

            // Listen for user input:
            string userEmail = Console.ReadLine();

            // Ensure that the user does not leave input blank:
            while(string.IsNullOrWhiteSpace(userEmail))
                {
                    // Inform the user their input was blank:
                    Console.Write("Oops! We didn't catch that, please enter an email address to be verified: ");

                    // Re-Listen user input:
                    userEmail = Console.ReadLine();
                }

            // Create variables to be sent to EmailChecker function / method and store returned value:
            string checkValid = EmailChecker(userEmail);
            

            if(checkValid == "valid")
                {
                    Console.WriteLine("The email address of {0} is a valid email address.", userEmail);
                }
            else if(checkValid == "invalid")
                {
                    Console.WriteLine("The email address of {0} is not a valid email address.", userEmail);
                }


            /*
            Data Sets to Test:
            --------------------------------------
            Let's verify if your entered email address is a proper address!
            Please enter an email address to be verified: test@fullsail.com
            The email address of test@fullsail.com is a valid email address.
            --------------------------------------
            Let's verify if your entered email address is a proper address!
            Please enter an email address to be verified: test@full@sail.com
            The email address of test@full@sail.com is not a valid email address.
            --------------------------------------
            Let's verify if your entered email address is a proper address!
            Please enter an email address to be verified: test@full sail.com
            The email address of test@full sail.com is not a valid email address.
            --------------------------------------
            Let's verify if your entered email address is a proper address!
            Please enter an email address to be verified: test@.com
            The email address of test@.com is not a valid email address.  // Changed loop to ensure that there is at least 1 char after the @ symbol as well.
            */

            // Problem #2 - Seperator Swap Out
            Console.WriteLine("\r\nProblem #2 - Seperator Swap Out\r\nLet's replace one type of seperator for another!");

            // Prompt the user for a list of items that are seperated:
            Console.Write("Please enter your list with seperators: ");

            // Listen for the user input:
            string userList = Console.ReadLine();

            // Ensure that the user does not leave input blank:
            while (string.IsNullOrWhiteSpace(userList))
                {
                    // Inform the user their input was blank:
                    Console.Write("Oops! We didn't catch that, please enter your list: ");

                    // Re-Listen user input:
                    userList = Console.ReadLine();
                }

            // Prompt the user which seperator they would like replaced:
            Console.Write("Which seperator would you like replaced? ");

            // Listen for the user input:
            string userReplace = Console.ReadLine();

            // Ensure that the user does not leave input blank:
            while (string.IsNullOrWhiteSpace(userReplace))
                {
                    // Inform the user their input was blank:
                    Console.Write("Oops! We didn't catch that, please enter the seperator you would like replaced: ");

                    // Re-Listen user input:
                    userReplace = Console.ReadLine();
                }

            // Prompt the user which seperator they want to used instead:
            Console.Write("Ok, what would you like to replace " + "\"" + userReplace + "\"" + " with? ");

            // Listen for the user input:
            string userNewSeperator = Console.ReadLine();

            // Ensure that the user does not leave input blank:
            while (string.IsNullOrWhiteSpace(userNewSeperator))
                {
                    // Inform the user their input was blank:
                    Console.Write("Oops! We didn't catch that, please enter the a seperator to replace " + "\"" + userReplace + "\"" + " with: ");

                    // Re-Listen user input:
                    userNewSeperator = Console.ReadLine();
                }

            // Create variable to store returned ReplacedSeperator values:
            string seperatorReplaced = ReplaceSeperator(userList, userReplace, userNewSeperator);

            // Output results to the user:
            Console.WriteLine("The original string of {0} with the new seperator is {1}.", userList, seperatorReplaced);

            /*
            Data Sets To Test:
            ----------------------------------------------------
            Let's replace one type of seperator for another!
            Please enter your list with seperators: 1,2,3,4,5
            Which seperator would you like replaced? ,
            Ok, what would you like to replace "," with? -
            The original string of 1,2,3,4,5 with the new seperator is 1-2-3-4-5.
            ----------------------------------------------------
            Let's replace one type of seperator for another!
            Please enter your list with seperators: red: blue: green: pink
            Which seperator would you like replaced? :
            Ok, what would you like to replace ":" with? ,
            The original string of red: blue: green: pink with the new seperator is red, blue, green, pink.
            ----------------------------------------------------
            Let's replace one type of seperator for another!
            Please enter your list with seperators: Matt - Christi - Jasmine - Phoebee - Bella
            Which seperator would you like replaced? -
            Ok, what would you like to replace "-" with? &
            The original string of Matt - Christi - Jasmine - Phoebee - Bella with the new seperator is Matt & Christi & Jasmine & Phoebee & Bella.
            ----------------------------------------------------
            */

        }

        public static string EmailChecker(string userInput)
        {

            // Defining variables to be used below
            int findAt = 0;
            int indexOfAt = userInput.IndexOf("@");
            int lastIndexOfPeriod = userInput.LastIndexOf(".");
           

            // Find out if email has any spaces:
            int findSpaces = userInput.IndexOf(" ");

            // Find out if email has only 1 @ symbol:
            foreach (char isAt in userInput)
            {
                // Compare each character in the userInput to see if it is an @. 
                // If it is, findAt increases by 1. Below, if 0 => findAt > 1 then it will return invalid.
                if(isAt == '@')
                    {
                        findAt++;
                    }
            }

            // Find out if userInput is a valid or invalid address and return value to main method:
            // if([There are no spaces] AND [There is only one @] AND [There is at least 1 . after the @ as well as 1 character]
            if (findSpaces == (-1) && findAt == 1 && lastIndexOfPeriod > indexOfAt + 1)
                {
                    // Return a value of valid:
                    string compare = "valid";
                    return compare;
                }
            else
                {
                    // Return a value of invalid:    
                    string compare = "invalid";
                    return compare;
                }

        }

        public static string ReplaceSeperator(string before, string oldSep, string newSep)
        {

            // Declare the string that will be contain the new seperator list and eventually returned:
            // It is empty because it needs to be started before the loop.
            string after = "";
            
            // Check each individual character to see if it is the old seperator.
            // If it is, replace it with the new one.
            // If it is not, simply add that character to the new string "after". 
            foreach(char index in before)
                {
                    // Temporary string variable to convert "index" to a string so that it can be compared with a string. 
                    // Could not find a way to compare a string with a char and did not want to use a regular for loop. 
                    string temp = index.ToString(); 
                    if(temp == oldSep)
                        {
                            after += newSep;
                        }
                    else
                        {
                            after += index;
                        }
                }
            // Return the completed new string:
            return after;

        }


    }
}
