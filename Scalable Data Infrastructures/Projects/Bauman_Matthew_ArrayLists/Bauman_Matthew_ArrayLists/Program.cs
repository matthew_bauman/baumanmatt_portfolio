﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bauman_Matthew_ArrayLists
{
    class Program
    {
        static void Main(string[] args)
        {

            /*
                Name: Matthew Bauman
                Date: 04/15/2017
                SDI Section 02
                ArrayLists Assignment
            */

            CustomArrayLists();

        }

        public static void CustomArrayLists()
        {

            // Create the first ArrayList of items:
            ArrayList familyNames = new ArrayList(5) { "Matt", "Christi", "Jasmine", "Phoebee", "Bella" };

            // Create the second ArrayList describing elements of the first:
            ArrayList familyRoles = new ArrayList(5) { "Dad", "Mom", "Sister", "Baby", "Dog" };


            // Descriptive Text:
            Console.WriteLine("Here are the original ArrayLists combined so that the matching index of each creates a sentence:\r\n");
            // Create a loop that cycles through both ArrayLists at the same time, combining each matching index into a sentence:
            for(int index = 0; index < familyNames.Count; index++)
                {
                    Console.WriteLine("{0} is the {1}.", familyNames[index], familyRoles[index]);
                }

            // Blank Line for formatting:
            Console.WriteLine("\r\n");

            // Remove 2 elements from each ArrayList
            familyNames.RemoveRange(2, 2);
            familyRoles.RemoveRange(2, 2);


            // Add 1 new element at the start of both ArrayLists:
            familyNames.Insert(0, "Laurie");
            familyRoles.Insert(0, "Grandma");

            // Descriptive Text:
            Console.WriteLine("Here is the modified ArrayLists after removing 2 elements and adding 1 to the beginning:\r\n");
            // Display modified arraylists:
            for(int index = 0; index < familyNames.Count; index++)
                {
                    Console.WriteLine("{0} is the {1}.", familyNames[index], familyRoles[index]);
                }

            /*
            Data Sets Outputted:
            Here are the original ArrayLists combined so that the matching index of each creates a sentence:

            Matt is the Dad.
            Christi is the Mom.
            Jasmine is the Sister.
            Phoebee is the Baby.
            Bella is the Dog.


            Here is the modified ArrayLists after removing 2 elements and adding 1 to the beginning:

            Laurie is the Grandma.
            Matt is the Dad.
            Christi is the Mom.
            Bella is the Dog.

            */



        }
    }
}
