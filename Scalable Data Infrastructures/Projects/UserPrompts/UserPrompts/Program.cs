﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UserPrompts
{
    class Program
    {
        static void Main(string[] args)
        {

            //ReadLine Method Of The Console
            //Method Is Something The Console Can Do
            //Reads The Next Line In The Console And Returns The Text String Back To Code

            //ONLY Returns A String Variable DataType

            //Ask The User Their Name

            //Ask The User The Questions First
            //Then "Listen" For The Response
            Console.WriteLine("Please Type In Your Name And Then Press Enter.");

            // "Listen" For The Answer
            //Create A Variable To "Catch" The Returned Text String
            string userName = Console.ReadLine();

            //Say Hello To User
            Console.WriteLine("Hello " + userName + " Welcome To Our Program");

            //Calculate The Perimeter Of A Rectangle
            // Width * 2 + Length * 2

            //Ask User For Width And Length

            //Tell The User What You Are Doing!
            Console.WriteLine("Let's Find Out The Perimeter Of A Rectangle!\r\nPlease Enter A Width And Press Enter");

            //Listen For The Width - This Is A Number In A Text String
            string widthString = Console.ReadLine();

            //Convert String To Number DataType
            double widthNumber = double.Parse(widthString);

            //Ask The User For Length
            Console.WriteLine("Now Please Enter A Length");

            //Store Answer As String
            string lengthString = Console.ReadLine();

            //Convert String To Int
            double lengthNumber = double.Parse(lengthString);

            //Use To Get Perimeter
            double perimeterRectangle = widthNumber * 2 + lengthNumber * 2;

            //Add In Descriptive Text
            Console.WriteLine("The Perimeter Of The Rectangle Is " + perimeterRectangle);

            //If user Types In Decimal Place Then We Have Issues


        }
    }
}
