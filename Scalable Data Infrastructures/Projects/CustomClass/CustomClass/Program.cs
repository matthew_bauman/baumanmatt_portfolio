﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CustomClass
{
    class Program
    {
        static void Main(string[] args)
        {

            // Create the first box:
            // Call the constructor function:
            Box firstBox = new Box(5, 5, 5, "Red");

            Box secondBox = new Box(6, 7, 8, "Blue");

            /* 
            
            Can the member variables be accessed directly?
            Do NOT make them public
            Console.WriteLine(firstBox.mHeight);
            firstBox.mHeight = 10;
            Console.WriteLine(firstBox.mHeight); 
            
             */

            // Access the information about the box by using a "GETTER" method.
            // Must be created in the custom class file. 

            // FirstBox:
            Console.WriteLine("The height of our first box is: " + firstBox.GetHeight());
            Console.WriteLine("The width of our first box is: " + firstBox.GetWidth());
            Console.WriteLine("The length of our first box is: " + firstBox.GetLength());
            Console.WriteLine("The Color of our first box is: " + firstBox.GetColor());

            // SecondBox:
            Console.WriteLine("\r\nThe height of our second box is: " + secondBox.GetHeight());
            Console.WriteLine("The width of our second box is: " + secondBox.GetWidth());
            Console.WriteLine("The length of our second box is: " + secondBox.GetLength());
            Console.WriteLine("The Color of our second box is: " + secondBox.GetColor());

            // Change the value of height of firstBox and secondBox:
            firstBox.SetHeight(-10);
            secondBox.SetHeight(2);

            Console.WriteLine("\r\nThe height of our first box after the setter is: " + firstBox.GetHeight());
            Console.WriteLine("The height of our second box after the setter is: " + secondBox.GetHeight());

            // Use the remaining SETTERS:
            firstBox.SetLength(10);
            firstBox.SetWidth(10);
            firstBox.SetColor("Yellow");

            Console.WriteLine("The width of our first box after the setter is: " + firstBox.GetWidth());
            Console.WriteLine("The length of our first box after the setter is: " + firstBox.GetLength());
            Console.WriteLine("The Color of our first box after the setter is: " + firstBox.GetColor());

            // Call on the custom class method FindVolume:
            int firstVolume = firstBox.FindVolume();
            Console.WriteLine(firstVolume);

            // Shortcut:
            Console.WriteLine("The volume of the first box is: " + firstBox.FindVolume());
            Console.WriteLine("The volume of the second box is: " + secondBox.FindVolume());

            // Call on the custom class method FindSurfaceArea
            Console.WriteLine("\r\nThe surface area of the first box is: " + firstBox.FindSurfaceArea());
            Console.WriteLine("The surface area of the second box is: " + secondBox.FindSurfaceArea());

            // Since these are methods within the custom class, they can be used over and over again. 
            firstBox.SetHeight(10);
            Console.WriteLine("The volume of the first box is: " + firstBox.FindVolume());

            // Call the constructor function with no color choice:
            Box thirdBox = new Box(6, 6, 6);

            // Call the getters:
            Console.WriteLine("\r\nThe Length of our third box is: " + thirdBox.GetLength());
            Console.WriteLine("The width of our third box is: " + thirdBox.GetWidth());
            Console.WriteLine("The height of our third box is: " + thirdBox.GetHeight());
            Console.WriteLine("The color of our third box is: " + thirdBox.GetColor());  // This returns an empty set because no value was passed in.

            // Set the color of the thirdBox:
            thirdBox.SetColor("Pink");

            Console.WriteLine("The color of our third box is: " + thirdBox.GetColor());


        }
    }
}
