﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CustomClass
{
    class Box
    {

        // Properties of the Box class go here:
        // Member Variable is the same as a property.
        // These are GLOBAL variables, but only for this class.
        // Member varibles are all private by default. They can be made "PUBLIC" by adding public in front of them. 

        // Length:
        int mLength;

        // Width:
        int mWidth;

        // Height:
        int mHeight;

        // Color:
        string mColor;

        // Create the constructor function:
        // Must start with access modifier "public".
        // Add paramters for the member variables:
        public Box(int _length, int _width, int _height, string _color)
        {
            // Set the values of the member variables using the parameters. 
            // Must redefine the values of the member variables.
            mLength = _length;
            mWidth = _width;
            mHeight = _height;
            mColor = _color;
            
        }

        // Method Overloading
        // Create the same function again by duplicating it with different parameters.
        // Can use a different number of parameters, or even different TYPES of parameters.
        // Create another constructor function with no color choice:
        public Box(int _length, int _width, int _height)
        {
            // Set the values of the member variables using the parameters:
            mLength = _length;
            mWidth = _width;
            mHeight = _height;

            // Adding a default color:
            mColor = "Gray";
        }

        // Create a GETTER method for the HEIGHT of the cube:
        // GETTERS should always be PUBLIC.
        // No parameters needed, just return a value. 

        public int GetHeight()
        {
            // Return the value of HEIGHT:
            return mHeight;
        
        }

        // Creating GETTER methods for the remaining variables:
        public int GetWidth()
        {
            // Return the value of WIDTH:
            return mWidth;
        }

        public int GetLength()
        {
            // Return the value of Length:
            return mLength;
        }

        public string GetColor()
        {
            // Return the value of Color
            return mColor;
        }

        // Create a SETTER for the HEIGHT:
        // This is a function and must be PUBLIC. Nothing is being returned, so VOID is used.
        // this.mHeight - "this" keyword refers to the current instance of the class. 
        public void SetHeight(int _height)
        {
            // Add a conditional to test of the value passed in is negative:
            if (_height > 0)
            {
                // Change the value of the member variable:
                this.mHeight = _height;
            }
            else
            {
                // Alert the user to not use negative numbers:
                Console.WriteLine("This value cannot be negative.\r\nPlease try again.");
            }
        }

        // Remaining SETTER functions:
        public void SetWidth(int _width)
        {
            // Change the value of the member variable:
            this.mWidth = _width;
        }

        public void SetLength(int _length)
        {
            // Change the value of the member variable:
            this.mLength = _length;
        }

        public void SetColor(string _color)
        {
            // Change the value of the member variable:
            this.mColor = _color;
        }

        // Create a custom method (Can be named whatever you want):
        // Does not need any parameters because the values will come from the member variables for each object.
        public int FindVolume()
        {
            // Use the member variables inside of our calculations:
            // Volume = Width * Length * Height
            int volume = mWidth * mLength * mHeight;
            return volume;
        }

        // Create a function for calculating the surface area:
        public int FindSurfaceArea()
        {
            // Use member variables in the calculations:
            // Surface Area = 2 * (Width * Length) + 2 * (Width * Height) + 2 * (Length * Height)
            int surfaceArea = 2 * (mWidth * mLength) + 2 * (mWidth * mHeight) + 2 * (mLength * mHeight);
            return surfaceArea;
        }



    }
}
