﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZombieLoopingMadness
{
    class Program
    {
        static void Main(string[] args)
        {



            // Zombie Maddness!

            // Givens first:
            // We start with 1 zombie.
            // It can bite 4 people in a day
            // How many in 8 days?

            // Take what you know and turn them into variables:

            // How many zombies do we have?
            int numZombies = 1; // We started with ONE

            // Number of bites per zombie, per day:
            int numBites = 4; // They can each bite 4 per day.

            // The number of days:
            int days = 8; 

            // Create a FOR LOOP to cycle through each day:
            for(int i = 1; i <= days; i++)
                {

                    // What happens in one day?
                    // How many NEW zombies get create in a day?
                    // Number of bites * number of zombies
                    int newZombies = numZombies + numBites;

                    // At the end of the day the new zombies join the horde
                    // Update number of Zombies:
                    numZombies += newZombies;

                    // Tell the public how many zombies we have each day:
                    Console.WriteLine("There are {0} zombies on day #{1}", numZombies, i); // {0} signifies the number of zombies and {1} is the FOR LOOP variable representing which day it is.

                }

            // How long it will take to reach a million zombies?
            int numDays = 1;   // We start with day 1, NOT day 0
            int zombieHordeNumber = 1;

            while(zombieHordeNumber <= 1000000)
                {
                    // Happens each day:
                    int bittenPeople = zombieHordeNumber * numBites;

                    // End of the day those people become zombies:
                    zombieHordeNumber += bittenPeople;

                    // Report to the people how many zombies there are:
                    Console.WriteLine("On day #{0}, there are {1} zombies!", numDays, zombieHordeNumber);

                    // End of the day, we need to increase the day number:
                    numDays++;

                }


        }
    }
}
