﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FindErrorsFunctions
{
    class Program
    {
        static void Main(string[] args)
        {
            // Name: Matthew Bauman
            // Date: 04/14/2017
            // Section: 02
            // Find The Errors In Functions

            // In this program we will be asking for 2 prices for the user.
            // We will ask for the sales tax rate.
            // Create a function that will return the price + sales tax.
            // Create a function that will add the 2 prices with sales tax together for the total cost.

            // Inform the user about the program:
            Console.WriteLine("Hello and welcome to our purchase calculator!\r\nWe will be asking you for 2 item prices and the sales tax rate.\r\n");

            // Prompt the user for the cost of their first item:
            Console.Write("What is the cost of your first item? $");

            // Listen for the cost of the first item:
            string cost1String = Console.ReadLine();

            // Declare a variable to hold the converted user string:
            decimal cost1Num;

            // Ensure the user entered a number and did not leave it blank:
            while (!decimal.TryParse(cost1String, out cost1Num))
                {
                    // Inform the user that their input is incorrect:
                    Console.WriteLine("Please only type in numbers!\r\nWhat is the cost of your first item?");
                    
                    // Re-listen for user input:
                    cost1String = Console.ReadLine();

                }

            
            // Prompt the user for the cost of their second item: 
            Console.Write("What is the cost of your second item? $");

            // Listen for the cost of the second item:
            string cost2String = Console.ReadLine();

            // Declare a variable to hold the converted user string:
            decimal cost2Num;

            // Ensure the user entered a number and did not leave it blank:
            while (!decimal.TryParse(cost2String, out cost2Num))
                {
                    // Inform the user that their input is incorrect:
                    Console.WriteLine("Please only type in numbers!\r\nWhat is the cost of your second item?");

                    // Re-listen for user input:
                    cost2String = Console.ReadLine();

                }


            // Prompt the user for the tax rate percentage:
            Console.Write("What is the sales tax rate %? ");

            // Listen for the user's input:
            string salesTaxString = Console.ReadLine();

            // Declare a variable to hold the converted user string:
            decimal salesTaxNum;

            // Ensure that the user entered a number and did not leave it blank:
            while (!decimal.TryParse(salesTaxString, out salesTaxNum))
                {
                    // Inform the user that their input is incorrect:
                    Console.WriteLine("Please only type in numbers!\r\nWhat is the sales tax rate in %?");

                    // Re-listen for user input:
                    salesTaxString = Console.ReadLine();

                }

            // Converting outputs into formatted strings:
            string forCost1Num = String.Format("{0:0.00}", cost1Num);
            string forCost2Num = String.Format("{0:0.00}", cost2Num);

            // Show the user the costs / percentage they entered. 
            Console.WriteLine("I have all the information I need.\r\nYour first item costs ${0}. Your second item costs ${1} and the sales tax is {2}%.", forCost1Num, forCost2Num, salesTaxNum);

            // Create Variable to store AddSalesTax function / method:
            decimal cost1WithTax = AddSalesTax(cost1Num, salesTaxNum);
            decimal cost2WithTax = AddSalesTax(cost2Num, salesTaxNum);

            // Create Variable to store TotalCosts function / method:
            decimal grandTotal = TotalCosts(cost1WithTax, cost2WithTax);

            // Converting outputs into formatted strings:
            string formattedCost1 = String.Format("{0:0.00}", cost1WithTax);
            string formattedCost2 = String.Format("{0:0.00}", cost2WithTax);
            string formattedGrandTotal = String.Format("{0:0.00}", grandTotal);

            // Output final values to the user:
            Console.WriteLine("\r\nWith tax your first item costs ${0}.\r\nWith tax your second item costs ${1}.", formattedCost1, formattedCost2);
            Console.WriteLine("\r\nWhich makes the total for your bill {0}", formattedGrandTotal);

            /*
            
            Data Sets Tested:
            ------------------------------------------

            What is the cost of your first item? $15
            What is the cost of your second item? $10
            What is the sales tax rate %? 8
            I have all the information I need.
            Your first item costs $15.00. Your second item costs $10.00 and the sales tax is 8%.

            With tax your first item costs $16.20.
            With tax your second item costs $10.80.

            Which makes the total for your bill 27.00

            ------------------------------------------

            What is the cost of your first item? $15.42
            What is the cost of your second item? $16.31
            What is the sales tax rate %? 6.5
            I have all the information I need.
            Your first item costs $15.42. Your second item costs $16.31 and the sales tax is 6.5%.

            With tax your first item costs $16.42.
            With tax your second item costs $17.37.

            Which makes the total for your bill 33.79

            -----------------------------------------
    
            */


        }

        public static decimal AddSalesTax(decimal price, decimal tax)
        {
            // Return price plus sales tax:
            decimal totalWithTax = price + (price * (tax / 100));
            return totalWithTax;

        }

        public static decimal TotalCosts(decimal cost1, decimal cost2)
        {
            // Return the total cost of both items with tax:
            decimal total = cost1 + cost2;
            return total; 

        }

    }
}

