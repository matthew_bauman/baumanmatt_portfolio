﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MoreArrays
{
    class Program
    {
        static void Main(string[] args)
        {
            // Function call to run our arrayList method / function:
            nameArrayLists();


        }

        public static void nameArrayLists()
        {
            // Create an ArrayList:
            // Add an initial element - This is completely optional.
            ArrayList studentList = new ArrayList() {"Matt"};


            // .Add() AND .Insert() Method ----------------------------------------------------------
            // Add some new students to our ArrayList:
            studentList.Add("Craig");
            studentList.Add("Josh");
            studentList.Add("Chad");
            studentList.Add("Rebecca");

            Console.WriteLine(studentList[0].ToString());

            // This will not work: 
            // Console.WriteLine("The array list so far is: " + studentList);

            // .Insert() (int, element) - "int" is the index location that "element" will be added to.
            studentList.Insert(0, "Lee");

            // .Remove() Method --------------------------------------------------------------------
            // If you try to remove something that is not in there the program will still function with no errors.
            // Remove a student:
            studentList.Remove("Chad");

            // .Clear() Method ---------------------------------------------------------------------
            // Clear the ArrayList:
            // studentList.Clear();

            // .Contains() Method ------------------------------------------------------------------
            // Returns a boolean of TRUE if element is in the array.
            // See if the ArrayList contains an element:
            Console.WriteLine("Contains?: {0}", studentList.Contains("Josh"));  // Returns TRUE
            Console.WriteLine("Contains?: {0}", studentList.Contains("Bob"));   // Returns FALSE 

            // Foreach loop to cycle through the array:
            foreach (string eachName in studentList)
                {
                    // Write out the names:
                    Console.WriteLine("Student: {0}", eachName);
                }

            // ArrayList Properties
            Console.WriteLine("\r\n");  // Blank line to seperate outputs.

            // Index # ----------------------------------------------------------------------------
            // Get a single element:
            Console.WriteLine(studentList[2]);

            // Set a single element:
            studentList[3] = "Chris";
            Console.WriteLine(studentList[3]);
            // Console.WriteLine(studentList[8]); - We do not have 9 elements, so there is no index of 8. 

            // .Count Method --------------------------------------------------------------------------
            // The number of elements that are ACTUALLY IN the ArrayList.
            // Get the number of elements in the ArrayList:
            Console.WriteLine("Count: {0}", studentList.Count);

            // .Capacity Method ----------------------------------------------------------------
            // The number of elements that the ArrayList CAN store, not the COUNT which is how many elements the ArrayList has. 
            Console.WriteLine("Capacity: {0}", studentList.Capacity);

            // Foreach loop to cycle through the array:
            foreach (string eachName in studentList)
                {
                    // Write out the names:
                    Console.WriteLine("Student: {0}", eachName);
                }


        }
    }
}
