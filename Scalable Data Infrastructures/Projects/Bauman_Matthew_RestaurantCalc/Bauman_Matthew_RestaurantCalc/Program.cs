﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bauman_Matthew_RestaurantCalc
{
    class Program
    {
        static void Main(string[] args)
        {

            /*
             Matthew Bauman
             SDI Section 02
             04/02/2017
             Restaurant Calculator
            */


            //Prompting The User For Their 3 Meal Prices And Tip
            Console.WriteLine("Thank You For Dining!");
            Console.WriteLine("Please Enter The Price Of The First Meal (Including Change):");

            //Listening For Meal 1 Price
            string mealOnePrice = Console.ReadLine();

            //Prompting User For Second Meal Price
            Console.WriteLine("Please Enter The Price Of The Second Meal (Including Change):");

            //Listening For Meal 2 Price
            string mealTwoPrice = Console.ReadLine();

            //Prompting User For Third Meal Price
            Console.WriteLine("Please Enter The Price Of The Third Meal (Including Change):");

            //Listening For Meal 3 Price
            string mealThreePrice = Console.ReadLine();

            //Prompting User For Tip Percentage
            Console.WriteLine("Please Enter Your Desired Tip Percentage:");

            //Listening For Tip Percentage
            string tipPercentage = Console.ReadLine();

            //Parsing User Inputs Into Numbers From Strings
            double mealOneParse = double.Parse(mealOnePrice);
            double mealTwoParse = double.Parse(mealTwoPrice);
            double mealThreeParse = double.Parse(mealThreePrice);
            double tipParse = double.Parse(tipPercentage);

            //Converting Tip To A Decimal
            double tipDecimal = tipParse / 100;

            //Calculating The Meal Totals
            double mealTotals = mealOneParse + mealTwoParse + mealThreeParse;

            //Converting Meal Totals Into A Formatted String
            string formattedMealTotals = String.Format("{0:0.00}", mealTotals);

            //Calculating Tip
            double mealTip = mealTotals * tipDecimal;
            mealTip = Math.Round(mealTip, 2);

            //Converting Tip Into A Formatted String
            string formattedMealTip = String.Format("{0:0.00}", mealTip);


            //Outputting Sub Total Price Of Meals And Tip To User
            Console.WriteLine("Thank You!");
            Console.WriteLine("Your Subtotal Today Is: $" + formattedMealTotals);
            Console.WriteLine("Your Tip Total Today Is: $" + formattedMealTip);

            //Calculating Grand Total / Final Cost WITH Tip
            double grandTotal = mealTotals + mealTip;
            grandTotal = Math.Round(grandTotal, 2);

            //Converting Grand Total Into Formatted String
            string formattedGrandTotal = String.Format("{0:0.00}", grandTotal);

            //Outputting Final Cost WITH Tip
            Console.WriteLine("Your Grand Total For Today Is: $" + formattedGrandTotal);

            //Determining Cost Per Person
            double costPerPerson = grandTotal / 3;
            Math.Round(costPerPerson, 2);

            //Converting Cost Per Person To Formatted String
            string formattedCostPerPerson = String.Format("{0:0.00}", costPerPerson);

            //Outputting Cost Per Person To User
            Console.WriteLine("Your Cost Per Guest Was $" + formattedCostPerPerson + " Today!");
            Console.WriteLine("Thank You And Please Come Again!");



            /*TEST VALUES
             
             Meal One = 10.00
             Meal Two = 11.00
             Meal Three = 12.00
             Tip Percentage = 10
             Tip Total = $3.30
             Subtotal = $33.00
             Cost Per Guest = $12.10

             Meal One = 9.99
             Meal Two = 14.97
             Meal Three = 7.95
             Tip Percentage = 12
             Tip Total = $3.95
             Subtotal = $32.91
             Cost Per Guest = $12.29
            
             
             
             
             */




        }
    }
}
