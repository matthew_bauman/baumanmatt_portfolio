﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FindErrorsClasses
{
    class Program
    {
        static void Main(string[] args)
        {

            /*
            Name: Matthew Bauman
            Date: 04/27/2017
            SDI Section 02
            Find the Errors Classes
            */

            // Declare variables that use the custom Theater class:
            Theater amcCineplex20 = new Theater("AMC Cineplex 20", 20, 10.00m);
            Theater regalCinema = new Theater("Regal Cinema", 15, 8.00m);

            // Output information to console:
            Console.WriteLine("Let's go see a movie at {0}!\r\nThey have {1} screens to choose from and the average ticket price is {2}.", amcCineplex20.GetName(), amcCineplex20.GetNumScreens(), amcCineplex20.GetTicketPrice().ToString("C"));
            Console.WriteLine("\r\nWhat about {0} instead?\r\nTheir average ticket price is only {1}.\r\nThe only drawback is that they only have {2} screens.", regalCinema.GetName(), regalCinema.GetTicketPrice().ToString("C"), regalCinema.GetNumScreens());

            // Calculate the total cost of tickets at Regal Cinema using the custom TotalTicketCost function:
            decimal totalRegal = regalCinema.TotalTicketCost(7);

            // Output information to console:
            Console.WriteLine("\r\nIf all 4 of us go to {0}, then that would bring the total cost to {1}.", regalCinema.GetName(), totalRegal.ToString("C"));
            Console.WriteLine("\r\nWait, I forgot I have a coupon for $3.00 off a movie at the {0}!", regalCinema.GetName());

            // Calculate the new TotalTicketCost after $3.00 coupon:
            decimal regalTicketCoupon = regalCinema.UseCoupon(3.00m);

            // Outputting the adjusted TotalTicketCost after coupon:
            Console.WriteLine("\r\nThat would make a ticket there cost only {0}.", regalTicketCoupon.ToString("C"));

            // Calculating the total cost of tickets at Regal Cinema minus coupon using overloaded TotalTicketCost Function:
            decimal totalRegalCoupon = regalCinema.TotalTicketCost(4, 3.00m);

            Console.WriteLine("\r\nWith your coupon, all 4 of us can go for only {0}!\r\nLet's go to {1}", totalRegalCoupon.ToString("C"), regalCinema.GetName());

        }
    }
}
