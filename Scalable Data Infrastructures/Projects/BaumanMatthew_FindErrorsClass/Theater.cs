﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FindErrorsClasses
{
    class Theater
    {
        // Define the Member Variables:
        string mTheaterName;
        int mNumberOfScreens;
        decimal mAverageTicketPrice;

        // Create the constructor function:
        public Theater(string _theaterName, int _numberOfScreens, decimal _averageTicketPrice)
        {

            // Use the incoming parameters to inialize our original member variables
            mTheaterName = _theaterName;
            mNumberOfScreens = _numberOfScreens;
            mAverageTicketPrice = _averageTicketPrice;

        }

        // Create the GETTER functions to return information to where it was called:
        public string GetName()
        {
            // Return the value of the Theater Name:
            return mTheaterName;
        }

        public int GetNumScreens()
        {
            // Return the value of the NumberOfScreens:
            return mNumberOfScreens;
        }

        public decimal GetTicketPrice()
        {
            // Return the value of the AverageTicketPrice:
            return mAverageTicketPrice;
        }

        // Creating SETTERS to change member variables:
        public void SetName(string _theaterName)
        {
            // Setting the name of the theater:
            this.mTheaterName = _theaterName;
        }

        public void SetNumScreens(int _numScreens)
        {
            // Setting the number of screens:
            this.mNumberOfScreens = _numScreens;
        }

        public void SetTicketPrice(decimal _ticketPrice)
        {
            // Setting the cost of the ticket price:
            this.mAverageTicketPrice = _ticketPrice;
        }


        // Custom function to return how much a certain #number of tickets will cost:
        public decimal TotalTicketCost(int _numberOfTickets)
        {
            // Calculating the total cost of the number of tickets bought:
            decimal totalCost = _numberOfTickets * mAverageTicketPrice;
            return totalCost;

        }

        // Overloading TotalTicketCost to calculate the total minus the use of a coupon:
        public decimal TotalTicketCost(int _numberOfTickets, decimal _couponTotal)
        {
            // Calculating the total cost of the number of tickets bought minus coupon:
            decimal totalCostCoupon = (_numberOfTickets * mAverageTicketPrice) - (_numberOfTickets * _couponTotal);
            return totalCostCoupon;
        }

        // Custom function to return ticket cost based on value of coupon:
        public decimal UseCoupon(decimal _couponTotal)
        {
            // Calculating the cost of a ticket after the use of a coupon:
            decimal couponCost = this.mAverageTicketPrice - _couponTotal;
            return couponCost;
        }

        
    }
}
