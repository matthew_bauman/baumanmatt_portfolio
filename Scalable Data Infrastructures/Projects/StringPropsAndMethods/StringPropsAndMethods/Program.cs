﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StringPropsAndMethods
{
    class Program
    {
        static void Main(string[] args)
        {

            // String Properties and Methods

            // Create a string variable:
            string food = "cheese pizza";

            // String property of length:
            // Length will tell you how many characters make up a text string.

            Console.WriteLine(food.Length);

            // String Methods

            // IndexOf(); -------------------------------------------------------------------------
            //Remember, the index number always starts with 0 so the returned value needs to be +1.
            // Will return -1 if it cannot find that string. 
            // This is case sensitive.

            Console.WriteLine(food.IndexOf("pizza", 3));

            string sentence = "Madam I'm Adam";

            int firstAdam = sentence.IndexOf("adam", 2);

            Console.WriteLine("Adam first occurs at index of {0}.", firstAdam);

            // LastIndexOf(); ---------------------------------------------------------------

            int lastAm = sentence.LastIndexOf("Ad");

            Console.WriteLine("The last index of Ad is {0}", lastAm);

            Console.WriteLine(sentence.ToLower());
            Console.WriteLine(sentence.ToUpper());

            string newSentence = sentence.ToLower();
            Console.WriteLine(newSentence);

            // SubString() ------------------------------------------------------------------
            string ultimateAnswer = "Life, The Universe and everything";

            // Find the first comma and take everything after:
            int firstComma = ultimateAnswer.IndexOf(",");
            Console.WriteLine(firstComma);

            string theSubString = ultimateAnswer.Substring(firstComma + 2,12);
            Console.WriteLine(theSubString);


            // Trim() ------------------------------------------------------------------------
            string groceryList = "Apple, Banana, Strawberry";
            Console.WriteLine(groceryList.Trim());

            // Find the first comma in the list:
            int firstCommaGroc = groceryList.IndexOf(",") + 1;
            Console.WriteLine(firstCommaGroc);

            // Find the next comma, which is the "last" comma:
            int lastCommaGroc = groceryList.LastIndexOf(",");
            Console.WriteLine(lastCommaGroc);

            // Find the length between commas
            int lengthOfItem = lastCommaGroc - firstCommaGroc;

            // Substring of the groceryList:
            string shortList = groceryList.Substring(firstCommaGroc, lengthOfItem).Trim();
            Console.WriteLine(shortList);

            // Split() -----------------------------------------------------------------------
            // This is the perfect method for taking a list and ordering it alphabetically. 

            // Split a string into an array of substrings:
            string teachersNames = "Williams, Dan; Carroll, Rebecca; Lewis, Lee; Wainman, Scott";

            // Create an array to hold the substrings:
            // Remember that CHAR use single quotes '' while STRING uses double "".
            string[] teacherArray = teachersNames.Split(';');

            // Cycle through the array using a FOREACH loop:
            foreach(string teacher in teacherArray)
                {
                    Console.WriteLine(teacher.Trim());
                }

            // Sort by alphabetical order:
            Array.Sort(teacherArray);

            Console.WriteLine("\r\n"); // Just adding a blank line.

            // Cycle through the array using a FOREACH loop:
            foreach (string teacher in teacherArray)
                {
                    Console.WriteLine(teacher.Trim());
                }

        }
    }
}
