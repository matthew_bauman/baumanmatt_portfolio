﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssignmentOperators
{
    class Program
    {
        static void Main(string[] args)
        {

            //Assignment Operators
            /*
            = Assignment Operator - It Means "Is"
            ++ Addition of 1 To The Variable - a++; Is The Same As a = a + 1;
            -- Subtraction of 1 To The Current Variable Value - a--; Is the Same As a = a - 1;
            += Addition Assignment Operator a += 4;  Same As  a = a + 4;
            -= Subtraction Assignment Operator  a-= 4;  Same As  a = a - 4;
            /= Division Assignment Operator  a /= 4;  Same As  a = a / 4;
            *= Multiplication Assignment Operator  a *= 4;  Same As  a = a * 4;

             */

            int toChange = 5; // Value Is 5
            toChange++; // Value Is Now 6 - Same Thing As toChange = toChange + 1;
            toChange--; // Value Is Now 5
            toChange += 3; // Value Is Now 8
            toChange -= 2; // Value Is Now 6
            toChange /= 2; // Value Is Now 3
            toChange *= 5; // Value Is Now 15

            //Longer Way
            toChange = toChange / 3;

            Console.WriteLine(toChange);


     

        }
    }
}
