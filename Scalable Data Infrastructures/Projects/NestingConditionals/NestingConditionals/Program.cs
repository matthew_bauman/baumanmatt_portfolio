﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NestingConditionals
{
    class Program
    {
        static void Main(string[] args)
        {

            // Nesting Conditionals 
            // Some decisions affect other decisions within your program.

            // If it is warm enough, lets go to the beach.
            // If not, let's see a movie. 
            // If the water is warm, lets go swimming.
            // If not, let's get a tan.

            // Create a temperature variable:
            int temp = 90;

            // Create a water temperature variable:
            int waterTemp = 78;

            // Test to see if we can go to the beach:
            if(temp >= 85)
                {

                    // It is warm enough, let's go to the beach output:
                    Console.WriteLine("It is warm enough, let's go to the beach!");

                    // Conditional block for water temp:
                    if (waterTemp >= 75)
                    {

                        Console.WriteLine("Let's go swimming!");

                    }
                    else
                    {

                        Console.WriteLine("The water is too cold, let's get a tan instead!");

                    }

            }
            else
                {

                    // It is too cold, let's go to the movies:
                    Console.WriteLine("It is too cold, let's go see a movie!");

                }


        }
    }
}
