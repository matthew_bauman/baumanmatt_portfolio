﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Comments_Console
{
    class Program
    {
        static void Main(string[] args)
        {
            //Single Lined Comment.


            /*
             * This is a multi-lined comment.
             * Just another Line
             * Can have as many of these as we want. 
             *
             */
            //Console.WriteLine() - will put a text string on to the console AND a new line character at the end of it!
            Console.WriteLine("This will print to the console.");
            Console.WriteLine("This is an example of a second line");

            //Console.Write() - No new line character.
            Console.Write("This is an example of a console write.\r\n");
            Console.Write("Another Line of text.");
            Console.WriteLine("Welcome to SDI");

            //Carriage Return \r
            //New Line Character \n
            //We will use for a new line character \r\n.
        }
    }

}

