﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bauman_Matthew_CustomClass
{
    class HitPoints
    {

        // Declaring the member variables:
        int mHighestHealth;
        int mLowestHealth;
        int mCurrentHealth;

        // Creating the constructor function:
        public HitPoints(int _highestHealth, int _lowestHealth, int _currentHealth)
        {

            mHighestHealth = _highestHealth;
            mLowestHealth = _lowestHealth;
            mCurrentHealth = _currentHealth;

        }

        // Creating GETTER functions to return information to where it was called:
        public int GetHighest()
        {
            // Returning the value of HighestHealth:
            return mHighestHealth;
        }

        public int GetLowest()
        {
            // Returning the value of LowestHealth:
            return mLowestHealth;
        }

        public int GetCurrent()
        {
            // Returning the value of CurrentHealth:
            return mCurrentHealth;
        }

        // Creating SETTER functions to change member variables:
        public void SetHighest(int _highestHealth)
        {
            // Setting the HighestHeatlth:
            this.mHighestHealth = _highestHealth;
        }

        public void SetLowest(int _lowestHealth)
        {
            // Setting the LowestHealth:
            this.mLowestHealth = _lowestHealth;
        }

        public void SetCurrent(int _currentHealth)
        {
            // Setting Current Health:
            this.mCurrentHealth = _currentHealth;
        }

        public int IncreaseHealth(int _increaseHealth)
        {
            mCurrentHealth = mCurrentHealth + _increaseHealth;
            return mCurrentHealth;
        }

        public int DecreaseHealth(int _decreaseHealth)
        {
            mCurrentHealth = mCurrentHealth - _decreaseHealth;
            return mCurrentHealth;
        }



    }
}
