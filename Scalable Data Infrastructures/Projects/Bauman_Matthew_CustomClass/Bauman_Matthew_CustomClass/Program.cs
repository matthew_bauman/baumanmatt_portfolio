﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bauman_Matthew_CustomClass
{
    class Program
    {
        static void Main(string[] args)
        {

            /*
                Name: Matthew Bauman
                Date: 04/29/2017
                SDI Section 02
                Custom Class Assignment
            */

            // Instantiating a new hero:
            HitPoints newHero = new HitPoints(100, 0, 100);

            // Variable to hold current health:
            int currentHealth = newHero.GetCurrent();

            // Variable to hold starting health:


            // Variable's to compare user's input:
            string checkPlus = "+";
            string checkMinus = "-";

            // Descriptive Text For User:
            Console.WriteLine("Welcome to Hero HitPoints! Let's decide what happens to our Hero's health shall we?");

            // Prompting User for Hero Name:
            Console.Write("Before we can start you must name your hero. Please enter a name: ");

            // Listening for user input:
            string heroName = Console.ReadLine();

            // Declaring a variable to check if user input just a number:
            double checkNum;

            // Validating that user entered a name and not a number, as well as did not leave input blank:
            while(string.IsNullOrWhiteSpace(heroName) || double.TryParse(heroName, out checkNum))
            {
                // Inform the user their input was incorrect:
                Console.Write("Whoops! Looks like you entered something other than a name. Please try again: ");

                // Re-Listen for user input:
                heroName = Console.ReadLine();

            }

            // Descriptive text for user:
            Console.WriteLine("\"{0}\"? That doesn't seem very heroic, but if you're sure...\r\nLet's begin!", heroName);
            Console.WriteLine("\r\nOk, so here is the deal. {0} is currently at 100 hit points and you get to decide what happens next.", heroName);
            Console.WriteLine("Here are the controls: Enter \"+\" to increase health or \"-\" to decrease health.");

            // Loop that will ask user if they want their hero's health to increase or decrease:
            for (int counter = 0; counter < 5; counter++)
            {
              
                // Ask if hit points should increase or decrease:
                Console.Write("Would you like to increase or decrease {0}'s hit points? ", heroName);

                // Listen for user's input:
                string userChoice = Console.ReadLine();

                // Checking user's input matches the requirements:
                while(string.IsNullOrWhiteSpace(userChoice) || !(userChoice == checkPlus || userChoice == checkMinus))
                {
                    // Inform the user their input was incorrect:
                    Console.Write("Whoops! Looks like you entered something other than \"+\" or \"-\". Please try again: ");

                    // Re-Listen for user input:
                    userChoice = Console.ReadLine();

                }


                // Ask for how much hit points should change:
                Console.Write("Please enter the number of hit points to calculate: ");

                // Listen for user input:
                string healthChangeString = Console.ReadLine();

                // Declaring variable to store converted user input:
                int healthChangeNum;

                // Checking user's input is a number and not blank and does not go above or below max / min health:
                while(string.IsNullOrWhiteSpace(healthChangeString) || !int.TryParse(healthChangeString, out healthChangeNum))
                {
                    // Inform user their input was incorrect:
                    Console.Write("Whoops! Looks like you entered something other than a number or you've caused {0}'s health to go below 0 or above 100\r\nPlease try again: ", heroName);

                    // Re-Listen for user input:
                    healthChangeString = Console.ReadLine();
                }


                    // Change health according to user input:

                    if (userChoice == checkPlus && newHero.GetCurrent() + healthChangeNum <= 100)
                    {
                        newHero.IncreaseHealth(healthChangeNum);
                        Console.WriteLine("{0}'s now has {1} hit points", heroName, newHero.GetCurrent());
                    }
                    else if (userChoice == checkMinus && newHero.GetCurrent() - healthChangeNum >= 0)
                    {
                        newHero.DecreaseHealth(healthChangeNum);
                        Console.WriteLine("{0}'s now has {1} hit points", heroName, newHero.GetCurrent());
                    }
                    else
                    {
                        Console.WriteLine("{0}'s health cannot exceed {1} or go below {2}. Please try again.", heroName, newHero.GetHighest(), newHero.GetLowest());
                    }
               

            }

            // Final Descriptive text for user:
            Console.WriteLine("\r\nGreat job! It takes true strength to enter + or - and some numbers 5 times...\r\nYour hero {0} started with 100 hit points and now has {1} hit points.\r\nThanks for playing!", heroName, newHero.GetCurrent());

        }
    }
}
