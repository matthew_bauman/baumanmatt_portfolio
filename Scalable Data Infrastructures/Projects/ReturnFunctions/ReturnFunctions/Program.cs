﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReturnFunctions
{
    class Program
    {
        static void Main(string[] args)
        {

            // Create the variables for width and height:
            int width1 = 10;
            int height1 = 5;


            // Function Call
            // Create a variable to "catch" the returned value:

            int results = CalcArea(width1, height1);

            Console.WriteLine(results);

            int results2 = CalcArea(3, 4);
            Console.WriteLine(results2);

        }

        public static int CalcArea(int w, int h)
        {

            // Inside of function CalcArea

            // Create 2 variables for width and length:
            // int width = 6;
            // int height = 7;

            // Calculate the area of a rectangle:
            // int area = width * height;

            int area = w * h;

            // Return the area:
            return area;



        }
    }

}
