﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrdersOfOperations
{
    class Program
    {
        static void Main(string[] args)
        {

            //Order Of Operations
            //PEMDAS - Please Excuse My Dear Aunt Sally
            //Parenthesis, Exponents, Multiply, Divide, Addition, Subtraction

            //Find The Average Of 4 Quizzes

            //Create Variables For The Quiz Values
            double quiz1 = 85;
            double quiz2 = 100;
            double quiz3 = 80;
            double quiz4 = 90;

            //Average Is Add Up All Variables Then Divide By Number Of Variables
            double average = (quiz1 + quiz2 + quiz3 + quiz4) / 4;

            //Add Descriptive Text
            Console.WriteLine("The Average Of The Quizzes is " + average);

            //Too Many Parenthesis Is Not A Good Thing - Makes It Harder To Read
            //Find The Perimeter Of A Rectangle
            // 2 * length + 2 * width

            //Create Variables
            int lengthRectangle = 8;
            int widthRectangle = 6;

            int perimeterRectangle = (2 * lengthRectangle) + (2 * widthRectangle);
            Console.WriteLine(perimeterRectangle);

        }
    }
}
