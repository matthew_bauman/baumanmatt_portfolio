﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bauman_Matthew_Methods
{
    class Program
    {
        static void Main(string[] args)
        {

            /*
            
            Name: Matthew Bauman
            Date: 04/14/2017
            SDI Section: 02
            Methods Assignment

            */

            // Problem #1: Painting A Wall:
            Console.WriteLine("Problem #1: Painting A Wall\r\nLet's calculate how many gallons it would take to paint a wall!");

            // Prompting the user for the WIDTH of the wall:
            Console.Write("Please enter the width of your wall in feet: ");

            // Listening for user input WIDTH:
            string widthString = Console.ReadLine();

            // Creating a variable to store the converted WIDTH value:
            double widthNum;

            // Confirm the user typed in a number (not a string), input is not negative, and input is not blank:
            while (!double.TryParse(widthString, out widthNum) || widthNum < 0)
                {

                    // Inform the user their input was incorrect:
                    Console.Write("Oops! Looks like you've entered something other than a positive number! Please try again: ");

                    // Re-capture the user input:
                    widthString = Console.ReadLine();

                }

            // Prompting the user for the HEIGHT of the wall:
            Console.Write("Please enter the height of your wall in feet: ");

            // Listening for user input height:
            string heightString = Console.ReadLine();

            // Creating a variable to store the converted WIDTH value:
            double heightNum;

            // Confirm the user typed in a number (not a string), input is not negative, and input is not blank:
            while (!double.TryParse(heightString, out heightNum) || heightNum < 0)
                {

                    // Inform the user their input was incorrect:
                    Console.Write("Oops! Looks like you've entered something other than a positive number! Please try again: ");

                    // Re-capture the user input:
                    heightString = Console.ReadLine();

                }

            // Prompting the user for the number of COATS OF PAINT on the wall:
            Console.Write("How many coats of paint are you going to use? ");

            // Listening for user input COATS OF PAINT:
            string coatsString = Console.ReadLine();

            // Creating a variable to store the converted COATS OF PAINT value:
            double coatsNum;

            // Confirm the user typed in a number (not a string), input is not negative, and input is not blank:
            while (!double.TryParse(coatsString, out coatsNum) || coatsNum < 0)
                {

                    // Inform the user their input was incorrect:
                    Console.Write("Oops! Looks like you've entered something other than a positive number! Please try again: ");

                    // Re-capture the user input:
                    coatsString = Console.ReadLine();

                }

            // Prompting the user for the SURFACE AREA of one gallon:
            Console.Write("What is the surface area, in square feet, that one gallon of your paint will cover? ");

            // Listening for user input SURFACE AREA:
            string areaString = Console.ReadLine();

            // Creating a variable to store the converted SURFACE AREA value:
            double areaNum;

            // Confirm the user typed in a number (not a string), input is not negative, and input is not blank:
            while (!double.TryParse(areaString, out areaNum) || areaNum < 0)
                {

                    // Inform the user their input was incorrect:
                    Console.Write("Oops! Looks like you've entered something other than a positive number! Please try again: ");

                    // Re-capture the user input:
                    areaString = Console.ReadLine();

                }

            // Text to display to user:
            Console.WriteLine("Okay great! Press enter to find out how many gallons you will need!");

            // Press enter to continue:
            // No need to assign a variable, we are not doing anything with this other than creating user interaction.
            Console.ReadLine();

            // Return value from PaintNeeded function:
            double gallonsNeeded = PaintNeeded(widthNum, heightNum, coatsNum, areaNum);

            Console.WriteLine("For {0} coat(s) on that wall, you will need {1} gallon(s) of paint.", coatsNum, gallonsNeeded);

            /*
            Data Sets Tested:
            ------------------------------------
            Let's calculate how many gallons it would take to paint a wall!
            Please enter the width of your wall in feet: 8
            Please enter the height of your wall in feet: 10
            How many coats of paint are you going to use? 2
            What is the surface area, in square feet, that one gallon of your paint will cover? 300
            Okay great! Press any key to find out how many gallons you will need!

            For 2 coats on that wall, you will need 0.53 gallons of paint.
            -------------------------------------
            Let's calculate how many gallons it would take to paint a wall!
            Please enter the width of your wall in feet: 30
            Please enter the height of your wall in feet: 12.5
            How many coats of paint are you going to use? 3
            What is the surface area, in square feet, that one gallon of your paint will cover? 350
            Okay great! Press any key to find out how many gallons you will need!

            For 3 coats on that wall, you will need 3.21 gallons of paint.
            -------------------------------------
            Let's calculate how many gallons it would take to paint a wall!
            Please enter the width of your wall in feet:
            Oops! Looks like you've entered something other than a positive number! Please try again: a
            Oops! Looks like you've entered something other than a positive number! Please try again: 14.2
            Please enter the height of your wall in feet:
            Oops! Looks like you've entered something other than a positive number! Please try again: a
            Oops! Looks like you've entered something other than a positive number! Please try again: 16.1
            How many coats of paint are you going to use?
            Oops! Looks like you've entered something other than a positive number! Please try again: a
            Oops! Looks like you've entered something other than a positive number! Please try again: 4
            What is the surface area, in square feet, that one gallon of your paint will cover?
            Oops! Looks like you've entered something other than a positive number! Please try again: a
            Oops! Looks like you've entered something other than a positive number! Please try again: 300
            Okay great! Press any key to find out how many gallons you will need!

            For 4 coats on that wall, you will need 3.05 gallons of paint.
            ----------------------------------------
            */

            // Problem #2 - Stung!
            Console.WriteLine("\r\nProblem# 2 - Stung!\r\nDid you know it takes 9 bee stings per pound an animal weighs to kill them?\r\nLets find out how many bee stings are needed to kill an animal!\r\n");

            // Prompt the user for the animals WEIGHT:
            Console.Write("What does the animal you wish death upon weigh (in pounds)? ");

            // Listen for user input WEIGHT:
            string weightString = Console.ReadLine();

            // Declare a variable to store the user input WEIGHT as a number:
            double weightNum;

            // Confirm the user typed in a number (not a string), input is not negative, and input is not blank:
            while (!double.TryParse(weightString, out weightNum) || weightNum < 0)
                {

                    // Inform the user their input was incorrect:
                    Console.Write("Sorry, that does not appear to be a valid number.\r\nWhat does the animal you wish death upon weigh (in pounds)? ");

                    // Re-capture the user input:
                    weightString = Console.ReadLine();

                }

            // Return value from KillAnimal function / method:
            double stingsNeeded = KillAnimal(weightNum);

            // Display results to the user:
            Console.WriteLine("It takes {0} bee stings to kill this animal.", stingsNeeded);

            /*
            Data Sets Tested:
            -------------------------------------------------
            What does the animal you wish death upon weigh (in pounds)? 10
            It takes 90 bee stings to kill this animal.
            -------------------------------------------------
            What does the animal you wish death upon weigh (in pounds)? 160
            It takes 1440 bee stings to kill this animal.
            -------------------------------------------------
            What does the animal you wish death upon weigh (in pounds)? Twenty
            Sorry, that does not appear to be a valid number.
            -------------------------------------------------
            What does the animal you wish death upon weigh (in pounds)? 1200
            It takes 10800 bee stings to kill this animal.
            -------------------------------------------------
            */

            // Problem #3 - Reverse It
            Console.WriteLine("\r\nProblem #3 - Reverse It\r\nLet's reverse an array!\r\n");

            // Declaring different arrays to be used in the data sets:
            ArrayList arrayOne = new ArrayList(5) { "apple", "pear", "peach", "coconut", "kiwi" };
            ArrayList arrayTwo = new ArrayList(7) { "red", "orange", "yellow", "green", "blue", "indigo", "violet" };
            ArrayList arrayThree = new ArrayList(5) { "Matt", "Christi", "Jasmine", "Phoebee", "Bella" };

            // Sending the three arrays to ReverseOrder:
            ArrayList resultsOne = ReverseOrder(arrayOne);
            ArrayList resultsTwo = ReverseOrder(arrayTwo);
            ArrayList resultsThree = ReverseOrder(arrayThree);

            // Displaying the results:
            // Attempted to display this exactly as the .PDF instructions showed it without hardcoding each array element into a Console.WriteLine
            // Could not get rid of the last comma - suggestions?
            Console.WriteLine("Array One: ");
            Console.Write("Your original array was [");
            foreach (string eachElement in arrayOne)
                {
                    // Write out the elements
                    Console.Write("\"" + eachElement + "\",");
                }
            Console.Write("] and now it is reversed as\r\n [");
            foreach (string eachElement in resultsOne)
                {
                    // Write out the elements
                    Console.Write("\"" + eachElement + "\",");
                }
            Console.WriteLine("]\r\n");

            Console.WriteLine("Array Two: ");
            Console.Write("Your original array was [");
            foreach (string eachElement in arrayTwo)
                {
                    // Write out the elements
                    Console.Write("\"" + eachElement + "\",");
                }
            Console.Write("] and now it is reversed as\r\n [");
            foreach (string eachElement in resultsTwo)
                {
                    // Write out the elements
                    Console.Write("\"" + eachElement + "\",");
                }
            Console.WriteLine("]\r\n");

            Console.WriteLine("Array Three: ");
            Console.Write("Your original array was [");
            foreach (string eachElement in arrayThree)
                {
                    // Write out the elements
                    Console.Write("\"" + eachElement + "\",");
                }
            Console.Write("] and now it is reversed as\r\n [");
            foreach (string eachElement in resultsThree)
                {
                    // Write out the elements
                    Console.Write("\"" + eachElement + "\",");
                }
            Console.WriteLine("]\r\n");


            /*
            Problem #3 - Reverse It
            Let's reverse an array!

            Array One:
            Your original array was ["apple","pear","peach","coconut","kiwi",] and now it is reversed as
             ["kiwi","coconut","peach","pear","apple",]

            Array Two:
            Your original array was ["red","orange","yellow","green","blue","indigo","violet",] and now it is reversed as
             ["violet","indigo","blue","green","yellow","orange","red",]

            Array Three:
            Your original array was ["Matt","Christi","Jasmine","Phoebee","Bella",] and now it is reversed as
             ["Bella","Phoebee","Jasmine","Christi","Matt",]

             */
       

        }

        public static double PaintNeeded(double w, double h, double c, double a)
        {

            // Calculate the surface area of the wall:
            double surArea = w * h;

            // Calculate the total surface area needed based on number of coats:
            double areaPlusCoats = surArea * c;


            // Calculate the number of gallons needed:
            double galNeed = areaPlusCoats / a;
            galNeed = Math.Round(galNeed, 2);
            return galNeed;
        }

        public static double KillAnimal(double pounds)
        {
            // Calculate AND return number of stings needed:
            double totalStings = pounds * 9;
            return totalStings;
        }

        public static ArrayList ReverseOrder(ArrayList originalArray)
        {
            // Create a new ArrayList to store reversed values:
            ArrayList reverseArray = new ArrayList();

            // Loop to reverse values:
            /*
            This loop is the shortest amount of code I could come up with that made this work. 
            I started with the variable index = 0 and named it "index" because it is what I used to determine the index number in which to grab a string from the originalArray
            and store it in the reverseArray. The loop will continue until it runs out of indices to go through and I defined that by using .Capacity to tell the loop
            how large the originalArray was. Each time the loop goes through the value of index increases by 1 in order to move to the next element in the originalArray.
            
            Inside the loop, the variable whichIndex takes the size of the originalArray and subtracts the index variable from it. So if there are 5 elements in originalArray
            and the index variable starts at 0, the first pass will grab the 5th element in the originalArray. I subtracted 1 from the .Capacity because index values 
            start at 0, not 1. 
            
            Finally, once the loop has grabbed the furthest index value, I use .Add to store the furthest element into the newly declared array. I originally also used .ToString
            but that was an un-needed step.
            */
            for (int index = 0; index < originalArray.Capacity; index++)
                {
                    // Declare a variable that will give the element number from the originalArray we want to store in the new array.
                    int whichIndex = (originalArray.Capacity - 1) - index;

                    // Adds in the element number defined by whichIndex:
                    reverseArray.Add(originalArray[whichIndex]);

                }
            return reverseArray;
        }
        
    }
}
