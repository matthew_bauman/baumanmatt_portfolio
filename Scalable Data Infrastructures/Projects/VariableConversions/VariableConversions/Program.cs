﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VariableConversions
{
    class Program
    {
        static void Main(string[] args)
        {

            //Implicit Conversions
            //Smaller Datatype To A Larger One

            short num = 23456;
            Console.WriteLine(num);

            //Implicitately Convert
            int bigNum = num;
            Console.WriteLine(bigNum);

            //Explicit Conversions
            //Be Careful And Watch For Data Loss

            double x = 1234.56;
            Console.WriteLine(x);

            //Convert To Int
            int xConverted = (int)x;
            Console.WriteLine(xConverted);

            //Convert A Large Number Into An sByte
            int z = 130;
            sbyte zConverted = (sbyte)z;

            Console.WriteLine(z);
            Console.WriteLine(zConverted);

            //Conversion Helper Classes

            //Convert Class
            string stringValue = "56";


            //Try To Multiple * 2
            //THIS DOES NOT WORK
            //int multiplied = stringValue * 2;

            //Convert String To Number Then Multiply
            int multiplied = Convert.ToInt32(stringValue);
            multiplied = multiplied * 2; // Same As Below
            multiplied *= 2; // Same As Above
            Console.WriteLine(multiplied);

            //Parse
            //Converts A String Version Into A Different DataType
            string salary = "150000";

            //Parse The String And Pull Out An Integer
            int salaryInt = int.Parse(salary);

            //Divide By 4 To Find Out Quartly Salary
            salaryInt = salaryInt / 4;

            Console.WriteLine(salaryInt);


        }
    }
}
