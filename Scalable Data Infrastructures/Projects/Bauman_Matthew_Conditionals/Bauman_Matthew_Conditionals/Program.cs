﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bauman_Matthew_Conditionals
{
    class Program
    {
        static void Main(string[] args)
        {

            //  NAME:  Matthew Bauman
            //  DATE:  04/09/2017
            //  Scalable Data Infrastructures Section 02
            //  Conditionals Assignment

            // Problem #1 - Temperature Converter 
            Console.WriteLine("Problem #1 - Temperature Converter");

            // What do I need?

            // Creating Fahrenheit and Celcius variables:
            string fahrenheit = "F";
            string celcius = "C";

            // Creating F and C number conversion variables:
            int fahrConverted;
            int celConverted;


            // Intial User Prompt:
            Console.Write("Please enter a temperature: ");

            // Listen for user input:
            string userTempString = Console.ReadLine();

            // Variable to store converted user input:
            int userTempNumber;

            // Validate that the user did enter - Reprompt if blank, also ensure that input is a number and not a string or word:
            while (string.IsNullOrWhiteSpace(userTempString) || !int.TryParse(userTempString, out userTempNumber))
                {
                    // Inform user that their input is blank or contains letters:
                    Console.Write("You have entered something other than a number, please try again: ");

                    // Re-Listen for user input:
                    userTempString = Console.ReadLine();

                }

            // Prompt user for temperature type (F or C):
            Console.Write("\r\nPlease enter F for Fahrenheit or C for Celsius: ");

            // Listen for user input:
            string userTempTypeString = Console.ReadLine();

            // Convert user input to uppercase lettering so that both upper and lower case letters are accept and can be used below:
            userTempTypeString = userTempTypeString.ToUpper();

            // Validate that user did not leave blank and that a "C" or "F" was entered:
            while(string.IsNullOrWhiteSpace(userTempTypeString) || !(userTempTypeString == fahrenheit || userTempTypeString == celcius))
                {

                // Inform the user their input was blank or incorrect:
                Console.Write("You have entered something other than F for Fahrenheight or C for Celsius\r\nPlease Try Again: ");

                // Re-Listen for user prompt:
                userTempTypeString = Console.ReadLine();
                userTempTypeString = userTempTypeString.ToUpper();

                }

            // Converting output to proper temperature temp and outputting to the user:
            if(userTempTypeString == fahrenheit)
                {
                    // Convert tempreature from fahrenheit to celcius and output to the user:
                    fahrConverted = (((userTempNumber - 32) * 5) / 9);
                    Console.WriteLine("{0}F is {1}C", userTempNumber, fahrConverted);
                }
            else if (userTempTypeString == celcius)
                {

                    // Convert temperature from celcius to fahrenheit and output to the user:
                    celConverted = ((userTempNumber * 9) / 5) + 32;
                    Console.WriteLine("{0}C is {1}F", userTempNumber, celConverted);     

                }

            /*
            Data Sets to Test:
            32F is 0C
            100C is 212F
            50c is 122F
            125f is 51C
            */

            // Problem #2 - Last Chance For Gas

            Console.WriteLine("\r\nProblem #2 - Last Chance For Gas");

            // What do I need?


            // Prompt user for first input (How many gallons of gas does their car hold):
            Console.WriteLine("Lets find out if you need to stop for gas now or if you can wait until the next station.");
            Console.Write("How many gallons does your car tank hold? ");

            // Listen for user input:
            string tankSizeString = Console.ReadLine();

            // Variable to store user to Try.Parse user input:
            double tankSizeNum;

            // Ensure user input is not blank and is a number and not a string:
            while (string.IsNullOrWhiteSpace(tankSizeString) || !double.TryParse(tankSizeString, out tankSizeNum))
                {
                    // Inform user that their input is blank or contains letters:
                    Console.Write("You have entered something other than a number, please try again: ");

                    // Re-Listen for user input:
                    tankSizeString = Console.ReadLine();

                }
           

            // Prompt user to find out how full the gas tank is in %:
            Console.Write("How full is your gas tank (In %): ");

            // Listen for user input:
            string tankPercentString = Console.ReadLine();

            // Second variable store Try.Parse:
            double tankPercentNum;

            // Ensure that input is not blank and is a number and not a string:
            while (string.IsNullOrWhiteSpace(tankPercentString) || !double.TryParse(tankPercentString, out tankPercentNum))
                {
                    // Inform user that their input is blank or contains letters:
                    Console.Write("You have entered something other than a number, please try again: ");

                    // Re-Listen for user input:
                    tankPercentString = Console.ReadLine();

                }
            

            // Prompt user for how many miles per gallon their car goes:
            Console.Write("How many miles per gallon does your car go? ");

            // listen for user input:
            string mpgString = Console.ReadLine();

            // Third variable to store Try.Parse
            double mpgNum;

            // Ensure that input is not blank and is a number and not a string:

            while (string.IsNullOrWhiteSpace(mpgString) || !double.TryParse(mpgString, out mpgNum))
                {
                    // Inform user that their input is blank or contains letters:
                    Console.Write("You have entered something other than a number, please try again: ");

                    // Re-Listen for user input:
                    mpgString = Console.ReadLine();

                }
           
            // Determine if user can make it at least 200 miles or if they need to stop now

            double milesLeft = tankSizeNum * (tankPercentNum / 100) * mpgNum; // Converting tankPercentNum into a percent here instead of declaring a new variable to hold it
            Console.WriteLine(milesLeft);
       

            if(milesLeft >= 200)
                {
                    Console.WriteLine("Yes, you can drive {0} more miles and you can make it without stopping for gas!", milesLeft);
                }
            else
                {
                    Console.WriteLine("You only have {0} you can drive, better get gas now while you can!", milesLeft);
                }

            /*
                Datasets to print out:
                Gallons - 20  Gas Tank - 50% full MPG - 20
                "Yes, you can drive 250 more miles and you can make it without stopping for gas!" 

                Gallons - 12, Gas Tank - 60% full MPG - 25
                "You only have 144 miles you can drive, better get gas now while you can!"

                Gallons 50, Gas Tank 20% full MPG - 18
                You only have 180 miles you can drive, better get gas now while you can!"

            */

            // Problem #3 - Grade Letter Calculator
            Console.WriteLine("\r\nProblem #3 - Grade Letter Calculator");

            // Prompting user for their course grade:
            Console.WriteLine("Let's determine what your letter grade for this course is.");
            Console.Write("Please enter your course percentage: ");

            // Listening for user input:
            string percentString = Console.ReadLine();

            // Variable to store converted string to an int:
            int percentNum;

            // Ensuring that their input is not blank and that they entered a number <= 100 and >= 0 and not a string:

            while (string.IsNullOrWhiteSpace(percentString) || !int.TryParse(percentString, out percentNum) || percentNum < 0 || percentNum > 100)
                {
                      // Inform user that their input is blank or contains letters or is not in correct range:
                    Console.Write("You have entered something other than a number between 0 and 100, please try again: ");

                    // Re-Listen for user input:
                    percentString = Console.ReadLine();
                }

            // Determining the user's course letter grade and outputting to the user:
            if (percentNum >= 90)
                {
                    Console.WriteLine("You have a {0}%, which means you have earned a(n) A in the class!", percentNum);
                }
            else if (percentNum >= 80)
                {
                    Console.WriteLine("You have a {0}%, which means you have earned a(n) B in the class!", percentNum);
                }
            else if (percentNum >= 73)
                {
                    Console.WriteLine("You have a {0}%, which means you have earned a(n) C in the class!", percentNum);
                }
            else if (percentNum >= 70)
                {
                    Console.WriteLine("You have a {0}%, which means you have earned a(n) D in the class!", percentNum);
                }
            else
                {
                    Console.WriteLine("You have a {0}%, which means you have earned a(n) F in the class!", percentNum);
                }
            /*
            Datasets to print out:
            Grade - 92 "You have a 92%, which means you have earned a(n) A in the class!"
            Grade - 80 "You have a 80%, which means you have earned a(n) B in the class!"
            Grade - 67 "You have a 67%, which means you have earned a(n) F in the class!"
            Grade - 150 "You have entered something other than a number between 0 and 100, please try again: "
            Grade - -10 "You have entered something other than a number between 0 and 100, please try again: "
            Grade = 73 "You have a 73%, which means you have earned a(n) C in the class!"
            */

            // Problem #4 - Discount Double Check
            Console.WriteLine("\r\nProblem #4 - Discount Double Check");

            // Prompt the user for the cost of the first item:
            Console.Write("Please enter the cost of your first item: $");

            // Listening for the cost of the first item:
            string firstItemString = Console.ReadLine();

            // Variable to store the converted user string:
            double firstItemNum;

            // Ensure that input is not blank and is a number and not a string:
            while (string.IsNullOrWhiteSpace(firstItemString) || !double.TryParse(firstItemString, out firstItemNum))
                {
                    // Inform user that their input is blank or contains letters:
                    Console.Write("You have entered something other than a number, please try again: ");

                    // Re-Listen for user input:
                    firstItemString = Console.ReadLine();

                }

            // Prompt the user for the cost of the second item:
            Console.Write("Please enter the cost of your second item: $");

            // Listening for the cost of the first item:
            string secondItemString = Console.ReadLine();

            // Variable to store the converted user string:
            double secondItemNum;

            // Ensure that input is not blank and is a number and not a string:
            while (string.IsNullOrWhiteSpace(secondItemString) || !double.TryParse(secondItemString, out secondItemNum))
                {
                    // Inform user that their input is blank or contains letters:
                    Console.Write("You have entered something other than a number, please try again: ");

                    // Re-Listen for user input:
                    secondItemString = Console.ReadLine();

                }

            // Adding 2 items up to determine if they get the discount:
            double purchaseTotal = firstItemNum + secondItemNum;
            double purchaseTotalTen = purchaseTotal / 10;
            double purchaseTotalFive = ((purchaseTotal * 5) / 100);
            double totalDiscount;


            if(purchaseTotal >= 100)
                {
                    totalDiscount = purchaseTotal - purchaseTotalTen;
                    totalDiscount = Math.Round(totalDiscount, 2);
                    Console.WriteLine("Your total purchase is ${0}, which includes your 10% discount.", totalDiscount);
                }
            else if(purchaseTotal >= 50)
                {
                    totalDiscount = purchaseTotal - purchaseTotalFive;
                    totalDiscount = Math.Round(totalDiscount, 2);
                    Console.WriteLine("Your total purchase is ${0}, which includes your 5% discount.", totalDiscount);
                }
            else
                {
                totalDiscount = purchaseTotal;
                    Console.WriteLine("Your total purchase is ${0}", totalDiscount);
                }

            /*
            Data Sets:

            Problem #4 - Discount Double Check
            Please enter the cost of your first item: $45.50
            Please enter the cost of your second item: $75.00
            Your total purchase is $108.45, which includes your 10% discount.

            Problem #4 - Discount Double Check
            Please enter the cost of your first item: $30.00
            Please enter the cost of your second item: $25.00
            Your total purchase is $52.25, which includes your 5% discount.

            Problem #4 - Discount Double Check
            Please enter the cost of your first item: $45.57
            Please enter the cost of your second item: $66.85
            Your total purchase is $101.18, which includes your 10% discount.
            Press any key to continue . . .

            */

        }
    }
}
