﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BasicExpressions
{
    class Program
    {
        static void Main(string[] args)
        {

            //Basic Expressions
            //Means Sequential Programming
            //Goes Line By Line

            //Declare AND Define A Variable
            int a = 2;

            //Change The Value Of A Permenantely
            a = a + 3;
            Console.WriteLine(a);

            //Create A New Variable And Base It On A Different Variable
            int b = a + 3;
            Console.WriteLine(b);
            Console.WriteLine(a);

            //Expression That Finds Our Age

            //We Need Current Year
            int currentYear = 2017;

            //Year You Were Born
            int yearBorn = 1989;

            //Simple Subtraction
            //Create A Variable To Hold The Age
            //If You Have NOT Had Your Birthday This Year, Subtract 1
            int age = currentYear - yearBorn - 1;

            Console.WriteLine("My Current Age Is " + age);

        
       

        }
    }
}
