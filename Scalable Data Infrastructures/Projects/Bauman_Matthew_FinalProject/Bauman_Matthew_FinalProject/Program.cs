﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bauman_Matthew_FinalProject
{
    class Program
    {
        static void Main(string[] args)
        {

            /*
                Name: Matthew Bauman
                Date: 04/29/2017
                SDI Section 02
                Final Project Assignment
            */

            // Declare a new TripTotal object:
            TripTotal userInfo = new TripTotal();

            // Welcome the user and explain purpose:
            Console.WriteLine("Welcome to TripTotal 1.0. This program will allow you to calculate the total cost of gasoline spent across multiple car trips.");

            // First User Prompt - Number of trips taken:
            Console.Write("\r\nLet's start with the number of trips you are going to take: ");

            // Listen For First User Prompt - Number of trips taken:
            string tripsTakenString = Console.ReadLine();

            // Variable to hold converted user input:
            int tripsTakenNum;

            // Loop to verify user input was valid:
            while (string.IsNullOrWhiteSpace(tripsTakenString) || !(int.TryParse(tripsTakenString, out tripsTakenNum)) || tripsTakenNum < 0)
            {
                // Inform the user that they have entered an invalid value:
                Console.Write("Whoops! Looks like you entered something other than a positive number. Please try again:");

                // Re-Listen for number of trips taken:
                tripsTakenString = Console.ReadLine();

            }

            // Setting the number of trips taken within userInfo using the user's input:
            userInfo.SetTrips(tripsTakenNum);           

            // Ask the user to input the user for each trip's mileage and store it. 
            for(int counter = 0; counter < userInfo.GetTrips() - 1; counter++)
            {
                // Prompt user for mileage:
                Console.Write("Please enter the total mileage for trip #{0}: ", counter + 1);

                // Listen for user's mileage:
                string mileageString = Console.ReadLine();

                // Create variable to hold converted user input:
                int mileageNum;

                // Ensure user input is valid:
                while (string.IsNullOrWhiteSpace(mileageString) || !(int.TryParse(mileageString, out mileageNum)) || mileageNum < 0)
                {
                    // Inform user their input is not valid:
                    Console.Write("Whoops! Looks like you entered something other than a positive number. Please try again: ");

                    // Re-listen for user's mileage:
                    mileageString = Console.ReadLine();
                }

                // Send user input to userInfo.GetMiles()
                userInfo.GetMiles(mileageNum);

                //Console.WriteLine(userInfo.GetTripLog().ToString());


            }
        }

    }
}
