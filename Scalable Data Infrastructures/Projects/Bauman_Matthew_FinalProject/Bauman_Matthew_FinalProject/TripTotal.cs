﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bauman_Matthew_FinalProject
{
    class TripTotal
    {

        // Declaring member variables:
        int mTotalTrips;
        int mTripMiles;
        ArrayList mUserTripLog;


        // Creating the construction function:
        public TripTotal()
        {
            // This simply exists so that users can initialize a TripTotal object without passing any parameters intially.
            // This is done incase the user wishes to intilize an object and then set the parameters based on final user input.
        }

        // Creating the first overloaded construction function:
        public TripTotal(int _totalTrips)
        {
            mTotalTrips = _totalTrips;
        }

        // Creating the second overloaded construction function:
        public TripTotal(int _totalTrips, int _tripMiles)
        {
            mTotalTrips = _totalTrips;
            mTripMiles = _tripMiles;
        }

        // Creating the third overloaded construction function:
        public TripTotal(int _totalTrips, int _tripMiles, ArrayList _userTripLog)
        {
            mTotalTrips = _totalTrips;
            mTripMiles = _tripMiles;
            mUserTripLog = _userTripLog;
        }

        // Creating GETTER functions:
        public int GetTrips()
        {
            // Returning the number of TotalTrips:
            return mTotalTrips;
        }

        public int GetTripMiles()
        {
            // Returning the number of TripMiles:
            return mTripMiles;
        }

        public ArrayList GetTripLog()
        {
            // Returning the ArrayList mUserTripLog:
            return mUserTripLog;
        }

        // Creating SETTER functions:
        public void SetTrips(int _totalTrips)
        {
            // Setting the total number of trips:
            this.mTotalTrips = _totalTrips;
        }

        public void SetMiles(int _tripMiles)
        {
            // Setting the trip miles:
            this.mTripMiles = _tripMiles;
        }

        public void SetTripLog(ArrayList _userTripLog)
        {
            mUserTripLog = _userTripLog;
        }

        // Custom function to return the current size of the the specified object's ArrayList:
        public int GetArraySize()
        {
            int arraySize = mUserTripLog.Capacity;
            return arraySize;
        }


        // Custom function to take in the total number of trips and return an array that holds the miles of each trip:
        public ArrayList GetMiles(int _userMiles)
        {
            // Modify the size of mUserTripLog to have as many elements as mTotalTrips
            // The size is based on the number of trips the user entered:
            mUserTripLog.Add(_userMiles);
            return mUserTripLog;
        }



    }
}
