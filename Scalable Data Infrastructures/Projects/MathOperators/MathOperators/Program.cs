﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MathOperators
{
    class Program
    {
        static void Main(string[] args)
        {

            //Math Operators
            //  + Addition With Numbers - Concatenation When Dealing With Text
            //  - Subtraction
            //  * Multiplication
            //  / Division

            //Area Of A Triangle
            //Width * Height /2

            int width = 8;
            int height = 7;

            int areaTriangle = (width * height) / 2;
            Console.WriteLine(areaTriangle);

            //Add Descriptive Text To Our Final Output
            Console.WriteLine("The Area Of A Triangle With A Width of " + width + " and a Height of " + height + " is " + areaTriangle);

            //Modulo Is Represented By % (Percent Sign)
            //Gives The Remainder Left Over
            decimal normalDivision = 36 / 10m;
            Console.WriteLine(normalDivision);

            int remainderValue = 15 % 7;
            Console.WriteLine(remainderValue);

            //Tell If Something Is Even Or Odd
            // %2 - If 1 Then It Is Odd. If It Is Zero Then It Is Even. 

            int evenOrOdd = 31 % 2;
            Console.WriteLine(evenOrOdd);



        }
    }
}
