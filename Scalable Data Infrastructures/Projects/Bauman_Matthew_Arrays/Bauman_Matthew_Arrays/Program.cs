﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Array_Assignment_Starting_File
{
    class Program
    {
        static void Main(string[] args)
        {
            /*
             Matthew Bauman
             SDI Section 02
             04/02/2017
             Arrays Assignment
             */

            //Create your own project and call it Lastname_Firstname_Arrays
            //Copy this code inside of this Main section into your Main Section
            //Work through each of the sections

            //Declare and Define The Starting Number Arrays
            int[] firstArray = new int[4] { 4, 20, 60, 150 };
            double[] secondArray = new double[4] { 5, 40.5, 65.4, 145.98 };

            //Find the total of each array and store it in a variable and output to console

            //Creating Variables To Store Array Totals
            int firstArrayTotal = firstArray[0] + firstArray[1] + firstArray[2] + firstArray[3];
            double secondArrayTotal = secondArray[0] + secondArray[1] + secondArray[2] + secondArray[3];

            //Outputting The Array Totals To The Console
            Console.WriteLine("The Total Of The First Array Is: " + firstArrayTotal);
            Console.WriteLine("The Total Of The Second Array Is: " + secondArrayTotal);


            //Calculate the average of each array and store it in a variable and output to console
            //Just a reminder to check the averages with a calculator as well, to make sure they are correct.

            //Converting First Array Total To A Double To Create Precise / Decimal Output
            double[] firstArrayConvert = new double[4] { firstArray[0], firstArray[1], firstArray[2], firstArray[3] };
       
            //Create The Variables To Store The Averages
            double firstArrayAverage = (firstArrayConvert[0] + firstArrayConvert[1] + firstArrayConvert[2] + firstArrayConvert[3]) / 4;
            double secondArrayAverage = (secondArray[0] + secondArray[1] + secondArray[2] + secondArray[3]) / 4;

            //Outputting The Array Averages To The Console
            Console.WriteLine("The Average Of The First Array Is: " + firstArrayAverage);
            Console.WriteLine("The Average Of The Second Array Is: " + secondArrayAverage);



            /*
               Create a 3rd number array.  
               The values of this array will come from the 2 given arrays.
                -You will take the first item in each of the 2 number arrays, add them together and then store this sum inside of the new array.
                -For example Add the index#0 of array 1 to index#0 of array2 and store this inside of your new array at the index#0 spot.
                -Repeat this for each index #.
                -Do not add them by hand, the computer must add them.
                -Do not use the numbers themselves, use the array elements.
                -After you have the completed new array, output this to the Console.
             */

            //Create Third Array With The Sum Of Each Of The Elements From Array One And Array Two
            double[] thirdArray = new double[4] { firstArray[0] + secondArray[0], firstArray[1] + secondArray[1], firstArray[2] + secondArray[2], firstArray[3] + secondArray[3] };

            //Output The Third Array To The Console
            Console.WriteLine("The Sum Of Index 0 Of The First and Second Array Is: " + thirdArray[0]);
            Console.WriteLine("The Sum Of Index 1 Of The First and Second Array Is: " + thirdArray[1]);
            Console.WriteLine("The Sum Of Index 2 Of The First and Second Array Is: " + thirdArray[2]);
            Console.WriteLine("The Sum Of Index 3 Of The First and Second Array Is: " + thirdArray[3]);




            /*
               Given the array of strings below named MixedUp.  
               You must create a string variable that puts the items in the correct order to make a complete sentence.
                -Use each element in the array, do not re-write the strings themselves.
                -Concatenate them in the correct order to form a sentence.
                -Store this new sentence string inside of a string variable you create.
                -Output this new string variable to the console.
             */

            //Declare and Define The String Array
            string[] MixedUp = new string[] { "but the lighting of a", "Education is not", "fire.", "the filling", "of a bucket," };


            //Declare + Define New String Variable To Create Correct Sentence
            string unMixedUp = MixedUp[1] + " " + MixedUp[3] + " " + MixedUp[4] + " " + MixedUp[0] + " " + MixedUp[2];

            //Outputting Correct Sentence To Console
            Console.WriteLine(unMixedUp);




        }
    }
}
