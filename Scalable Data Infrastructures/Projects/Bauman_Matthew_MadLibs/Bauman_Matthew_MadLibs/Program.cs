﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bauman_Matthew_MadLibs
{
    class Program
    {
        static void Main(string[] args)
        {

            /*
            Matthew Bauman
            SDI Section 02
            04/02/2017
            MadLibs
            */

            /*
            Notes And Questions For Future Development

            How Would You Reprompt The User If They Don't Enter A Number Value Within The Correct Range?

            How Would You Reprompt The User If They Enter More Than One Word On Certain Word / String Inputs?

            Is There A Way To Uniformly Format Final Output So That Lines Are The Same Length? Or A More Efficent
            Way To Output The Lines Other Than Multiple Console.WriteLine(); Commands?

            You Cannot Underline User Inputs In Final Output, So I Found Out How To Format It To All Caps,
            But Is There A Way To Change Text Color Or Any Other Way To Make Them Stand Out Within Descriptive Text?
            */


            //Beginning Of Console UI And Word Prompts
            Console.WriteLine("Welcome To Matt's Mad Libs!");
            Console.WriteLine("Let's Start With Words First.");
            Console.Write("Please Enter A The Name Of A Company: ");

            //Listening For First Word Prompt (Company Name)
            //Formatting The String To All Caps So User Can See Their Input More Clearly
            string firstWordCompany = Console.ReadLine();
            firstWordCompany = firstWordCompany.ToUpper();

            //Second Word Prompt (Adjective)
            Console.Write("Great! Now Enter An Adjective: ");

            //Listening For Second Word Prompt (Adjective)
            //Formatting The String To All Caps So User Can See Their Input More Clearly
            string secondWordAdjective = Console.ReadLine();
            secondWordAdjective = secondWordAdjective.ToUpper();

            //Third Word Prompt (Adjective Two)
            Console.Write("Perfect! Now Lets Enter Another Adjective: ");

            //Listening For Third Word Prompt (Adjective Two)
            //Formatting The String To All Caps So User Can See Their Input More Clearly
            string thirdWordAdjective = Console.ReadLine();
            thirdWordAdjective = thirdWordAdjective.ToUpper();

            //Fourth Word Prompt (Plural Noun)
            Console.Write("Wow...This Is Already Hillarious! Lets Enter A Plural Noun Now: ");

            //Listening For Fourth Word Prompt (Plural Noun)
            //Formatting The String To All Caps So User Can See Their Input More Clearly
            string fourthWordPluralNoun = Console.ReadLine();
            fourthWordPluralNoun = fourthWordPluralNoun.ToUpper();

            //Fifth Word Prompt (Verb)
            Console.Write("We Are Almost There! Now Enter A Verb: ");

            //Listening For Fifth Word Prompt (Verb)
            //Formatting The String To All Caps So User Can See Their Input More Clearly
            string fifthWordVerb = Console.ReadLine();
            fifthWordVerb = fifthWordVerb.ToUpper();

            //Sixth Word Prompt (Website URL)
            Console.Write("Ok, Last One! Please Enter A Website URL (Ex. Google.com): ");

            //Listening For Sixth Word Prompt (Webiste URL)
            //Formatting The String To All Caps So User Can See Their Input More Clearly
            string sixthWordURL = Console.ReadLine();
            sixthWordURL = sixthWordURL.ToUpper();

            //Begin Number Prompts

            //Prompting For First Number (1-10)
            Console.WriteLine("We Are Now Finished With Words So Lets Enter Some Numbers");
            Console.Write("Please Enter A Number Between 1 And 10: ");

            //Listening For First Number (1 - 10)
            string firstNumber = Console.ReadLine();

            //Prompting For Second Number (20 - 30)
            Console.Write("Now Lets Enter A Number Between 20 and 30: ");

            //Listening For Second Number (20-30)
            string secondNumber = Console.ReadLine();

            //Prompting For Third Number (Any)
            Console.Write("And Finally, Please Enter ANY Number: ");

            //Listening For Third Number (Any)
            string thirdNumber = Console.ReadLine();

            //Converting User Number Inputs From String To Numbers
            double firstDouble = double.Parse(firstNumber);
            double secondDouble = double.Parse(secondNumber);
            double thirdDouble = double.Parse(thirdNumber);

            //Declaring AND Defining An Array To Hold Parsed Values
            double[] numberArray = new double[3] {firstDouble, secondDouble, thirdDouble };

            //Descriptive Text And User Prompt
            Console.WriteLine("");
            Console.WriteLine("Ok That's Everything We Need. Press Enter When You Are Ready To Laugh!");
            Console.ReadLine();

            //Final Output With Strings And Array Values Present
            Console.WriteLine("");
            Console.WriteLine("More and more people are getting their news from " + firstWordCompany);
            Console.WriteLine("due to increasing amounts of " + secondWordAdjective + " news.");
            Console.WriteLine("It is estimated that " + numberArray[0] + " out of every " + numberArray[1]);
            Console.WriteLine("news stories are " + thirdWordAdjective + " because they are written by");
            Console.WriteLine(fourthWordPluralNoun + "! If just " + numberArray[2] + " people joined the fight,");
            Console.WriteLine("we could " + fifthWordVerb + " this problem forever. For more");
            Console.WriteLine("info please visit " + sixthWordURL + " today!");   
                

        }
    }
}
