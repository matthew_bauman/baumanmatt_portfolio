﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ValidatingUserPrompts
{
    class Program
    {
        static void Main(string[] args)
        {

            // Validating User Prompts with Condtionals

            // Ask the user for their name:
            Console.Write("Hello, please type in your name and press enter: ");

            // Store the results in a string variable:
            string userName = Console.ReadLine();

            // Test to see if the user left it blank
            // string.IsNullOrWhiteSpace( Variable to check here );
            // Returns a boolean value of TRUE if it is blank.
            // Returns a boolean value of FALSE if it contains anything other than white space.

            // Console.WriteLine(string.IsNullOrWhiteSpace(userName));

            // Create a conditional to test if the user inputted anything:
            if(string.IsNullOrWhiteSpace(userName))
                {
                    // If this is TRUE then the user left the prompt blank.
                    // We must tell the user what went wrong

                    Console.Write("Please do not leave this blank. Enter your name: ");

                    // Capture the response again:
                    userName = Console.ReadLine();

                }

            Console.WriteLine("Hello " + userName + "! Thanks for running my program!");

            Console.Write(" \r\nPlease enter an integer between 1 and 10: ");
            string userIntString = Console.ReadLine();

            // Convert the above user input to an integer datatype:
            // Instead of using Parse, we will be using TryParse now!
            // TryParse int.TryParse( variable to try to convert, out variable name to hold the converted number);
            // TryParse returns a boolean value of TRUE if the conversion works. It returns FALSE if there is an error.
            // It will not cause a runtime error. It also stores the converted value in a defined variable if possible. 

            // Declare a variable to hold the number that is being converted:
            int userInt;

            // Create a conditional to test validation:
            if(!(int.TryParse(userIntString, out userInt)))
                {
                // This will run if it could NOT convert it.
                // Tell the user the problem with their input:
                Console.WriteLine("You typed in something other than an integer between 1 and 10.\r\nPlease try again: ");

                //Re-Capture the user response - use the same string as above:
                userIntString = Console.ReadLine();

                //Have to RE-Convert it to an integer:
                int.TryParse(userIntString, out userInt);

                }

            Console.WriteLine(int.TryParse(userIntString, out userInt));


            Console.WriteLine(userInt * 2);



        }
    }
}
