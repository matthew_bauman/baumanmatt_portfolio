﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdvancedStringConcatenation
{
    class Program
    {
        static void Main(string[] args)
        {

            double myNum = 12.345;
            Console.WriteLine("The value is " + myNum);

            // User a placeholder or paramater {index#]
            // Index starts at 0.
            string test = String.Format("The meaning of life is {0}." , 42);
            Console.WriteLine(test);

            double otherNum = 56.7;

            Console.WriteLine("The first number is {0} and the second number is {1}.", myNum, otherNum);

            /*
              
                Going forward you can use either method of concatenation. You can use the originally taught method of the PLUS (+) symbol
                or you can create a parameter and output that as well.

             
            */
           
        }
    }
}
