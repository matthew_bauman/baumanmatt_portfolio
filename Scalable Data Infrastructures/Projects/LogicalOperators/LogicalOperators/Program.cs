﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogicalOperators
{
    class Program
    {
        static void Main(string[] args)
        {

            // Logical Operators
            // "AND" Operator &&
            // Can We Purchase An iPhone?
            decimal budget = 600.00m;
            decimal iPhoneCost = 500.00m;
            decimal payCheck = 500.00m;

            // We Can Buy The Phone Is Less Than BUDGET And Our Paycheck is Greater Than 700 To Cover Our Bills

            if(iPhoneCost < budget && payCheck >= 700)
                {
                    // We Can Buy The Phone
                    Console.WriteLine("You Can Purchase The iPhone");
                }
            else
                {
                Console.WriteLine("Sorry You Cannot Buy The iPhone");
                }

            // We Can Buy The Phone Is Less Than BUDGET OR Our Paycheck is Greater Than 700 To Cover Our Bills
            // OR (||) Operator
            if (iPhoneCost < budget || payCheck >= 700)
                {
                    // We Can Buy The Phone
                    Console.WriteLine("You Can Purchase The iPhone");
                }
            else
                {
                    Console.WriteLine("Sorry You Cannot Buy The iPhone");
                }

            //NOT Operator (!)
            if(!(iPhoneCost > budget))
                {
                    Console.WriteLine("From The Not Operator You Can Buy The Phone");
                }
            else
                {
                Console.WriteLine("From The Not Operator You Can NOT Buy The Phone");
                }
        
        }
    }
}
