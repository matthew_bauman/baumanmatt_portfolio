﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WhileLoops
{
    class Program
    {
        static void Main(string[] args)
        {

            // While Loops.

            // Declare and define a starting counting variable:
            int counter = 20;

            while(counter > 0)
                {
                    Console.WriteLine("The counter is {0}.", counter);

                    // Increment of change to avoid infinite loops:
                    counter--;
                }

            // Start of verifying user input with while loops:
            Console.WriteLine("Please Enter Your Name: ");

            // Listen for user input:
            string userName = Console.ReadLine();

            // Validate that the user did not leave it blank:
            // IsNullOrWhiteSpace for text strings:

            while (string.IsNullOrWhiteSpace(userName))
                {
                    // Tell the user what is happening and what is wrong:
                    Console.WriteLine("Please do not leave input blank. \r\nPlease type in your name:");

                    // Re-Define the variable to save response (already defined):
                    userName = Console.ReadLine();
                }

            // Convert user input to a number from a string and then validate:
            Console.WriteLine("Hi {0}! Please enter a number: ", userName);

            // Save response:
            string numberString = Console.ReadLine();

            // Declare variable for Try.Parse to store converted value:
            double userNum;

            // While loop validation process.
            while(!double.TryParse(numberString, out userNum))
                {
                // Tell the user the problem:
                Console.WriteLine("You have typed something other than a number. \r\nPlease enter a number: ");

                // Re-Capture user input:
                numberString = Console.ReadLine();

                }

            // Test to ensure the code is working properly:
            Console.WriteLine("The number that you typed in is {0}" \r\n, userNum*2); // Multiplication proves it is actually using a number and not a string. 


            // Do While Loops 

            // Declare and define the variable to test:
            // I is common, stands for Iteration:
            int i = 0;

            // Do While loops always run at least one time and THEN do the check.
            do
            {
                Console.WriteLine("The value of i is {0}.", i);
                // Increment of Change
                i++;
            } while (i < 5);



        }
    }
}
