﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ComicBookProject
{
    class ComicBook
    {

        // Create Member Variables to describe the comic books:
        string mTitle;
        int mYearPublished;
        decimal mOriginalCost;
        decimal mCurrentValue;

        // Create the constructor function:
        public ComicBook(string _title, int _yearPublished, decimal _originalCost, decimal _currentValue)
        {

            // Use the incomming parameters to intialize our original member variables:
            mTitle = _title;
            mYearPublished = _yearPublished;
            mOriginalCost = _originalCost;
            mCurrentValue = _currentValue;

        }

        // Creating GETTERS to return information to where it was called:
        public string GetTitle()
        {
            // Return the value of the title:
            return mTitle;
        }

        public int GetYearPublished()
        {
            // Return the value of the YearPublished:
            return mYearPublished;
        }

        public decimal GetOriginalCost()
        {
            // Return the value of OriginalCost:
            return mOriginalCost;
        }

        public decimal GetCurrentValue()
        {
            // Return the value of CurrentValue:
            return mCurrentValue;
        }


        // Creating SETTERS to change member variables
        public void SetTitle(string _title)
        {
            // Change the member variable TITLE and use the parameter:
            this.mTitle = _title;
        }

        public void SetYearPublished(int _yearPublished)
        {
            // Change the member variable YearPublished and use parameter:
            this.mYearPublished = _yearPublished;
        }

        public void SetOriginalCost(decimal _originalCost)
        {
            // Change the member variable OriginalCost and use parameter:
            this.mOriginalCost = _originalCost;
        }

        public void SetCurrentValue(decimal _currentValue)
        {
            // Change the member variable CurrentValue and use parameter:
            this.mCurrentValue = _currentValue;
        }

        // Create a custom method:
        // Calculate the Net Worth (How much paid vs. current value and find the difference).
        // Return it as a formatted text string.
        public string NetWorth()
        {

            // Calculate the net worth:
            decimal netWorth = mCurrentValue - mOriginalCost;

            // Convert the decimal into a formatted text string using ToString():
            string netWorthFormatted = netWorth.ToString("C");

            // Return formatted text string:
            return netWorthFormatted;

        }

        // Create a method that tells us how old the comic book is:
        // Take in an arguement of the current year and subtract the year published.
        public int Age(int _currentYear)
        {

            // Find the age:
            int age = _currentYear - mYearPublished;
            return age;

        }

    }
}
