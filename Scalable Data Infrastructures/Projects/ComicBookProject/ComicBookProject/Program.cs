﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ComicBookProject
{
    class Program
    {
        static void Main(string[] args)
        {

            // Create an instance of the ComicBook class:
            // Creates an object with all the info attached to it.
            ComicBook avengers = new ComicBook("Avengers Annual #16", 1987, 1.25m, 4.00m);

            // Output the name of the comic book to the console:
            Console.WriteLine("The title of my first comic book is: " + avengers.GetTitle());

            // Output the price of the comic book to the console:
            Console.WriteLine("The current value of my first comic book is: " + avengers.GetCurrentValue());

            // Change the current value of the comic:
            avengers.SetCurrentValue(20.00m);

            Console.WriteLine("The current value of my comic book after the movie came out is: " + avengers.GetCurrentValue());

            // Create a second comic book:
            ComicBook spiderMan = new ComicBook("The Amazing Spiderman #194", 1979, 0.40m, 2000.00m);

            Console.WriteLine("The first appearance of Black Cat is in " + spiderMan.GetTitle());

            // Find out the net worth of the spiderMan comic:
            Console.WriteLine("The current net worth of the Spiderman comic is: " + spiderMan.NetWorth());

            // Change the current value of the comic book:
            spiderMan.SetCurrentValue(100.00m);

            Console.WriteLine("The current networth of the Spiderman comic after the damage is: " + spiderMan.NetWorth());

            // Find out the age of the comic:
            // .Age() is expecting the current year as a parameter based on the way the method within ComicBooks was written.
            Console.WriteLine("The current age of the Avengers comic book is: " + avengers.Age(2017)); 

        }
    }
}
