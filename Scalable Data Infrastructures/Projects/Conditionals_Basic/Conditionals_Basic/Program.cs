﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Conditionals_Basic
{
    class Program
    {
        static void Main(string[] args)
        {

            // Boolean Variables
            // Only Contain A Value Of True Or False

            // True Is NOT A Text String Of "True"
            // Think Of This As "On"
            bool testOne = true;

            bool wontRun = false;

            //Relational Operators
            Console.WriteLine(5 < 7); // True Statement
            Console.WriteLine(7 > 5); // True Statement, Same Logically As Above

            Console.WriteLine(2 > 5); // False Statement

            Console.WriteLine(5 <= 5); // True Statement
            Console.WriteLine(6 < 6); // False Statement

            Console.WriteLine(4 == 4); // True Statement
            // Console.WriteLine(4 == "4"); Cannot Compare Different Datatypes

            Console.WriteLine(4 != 3); // True Statement

            Console.WriteLine(4 != 4); // False Statement

            // If Statement
            // Else Statement

            // Declare AND Define Variable For Child's Age
            int kidAge = 13;

            if (kidAge >= 13)
                {
                    Console.WriteLine("The Child Can Go To The PG 13 Movie");    
                    
                }
            else if(kidAge >= 10)
                {
                    Console.WriteLine("The Child Can Go To The PG Movie");
                }
            else
                {
                    Console.WriteLine("The Child Can Only See G Rated Movies");
                }



            // We Work In A Shoe Store
            // We Get A Bonus IF We Sell 50 OR MORE Pairs Of Shoes In A Day

            // Declare And Define Starting Variables
            decimal basePay = 200.00m;
            decimal bonus = 50.00m;

            // What Is Our Total Pay At The End Of The Day?
            decimal totalPay = 0.00m;

            // How Many Pairs Of Shoes Did We Sell In A Day?
            int shoesPerDay = 55;

            // Create A Conditional To Test If We Get Our Bonus

            if(shoesPerDay >= 50)
                {
                    // We Get Our Bonus
                    // Total Pay Should Be Base Pay Plus Bonus Pay
                    totalPay = basePay + bonus;
                }
            else
                {
                    // We Do NOT Get A Bonus
                    // Total Pay Is Same As Base Pay
                    totalPay = basePay;
                }

            // Report To The User Their Total Pay That Day
            Console.WriteLine("Your Total Pay Is $" + totalPay);

            
        


        }
    }
}
