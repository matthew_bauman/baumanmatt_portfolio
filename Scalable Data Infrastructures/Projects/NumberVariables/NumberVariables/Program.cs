﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NumberVariables
{
    class Program
    {
        static void Main(string[] args)
        {

            //Numeric Data Types
            //Integers
            //sbyte - Signed Byte - Positive or Neg
            sbyte exSbyte = 50;
            Console.WriteLine(exSbyte);

            //Short
            short exShort = 1000;
            Console.WriteLine(exShort);

            //Integer - int
            int exInt = 6000000;
            Console.WriteLine(exInt);

            //Long
            long exLong = 400000000;
            Console.WriteLine(exLong);


            //Double - Default / Standard
            double exDouble = 67.1234;
            Console.WriteLine(exDouble);

            //Float
            float exFloat = 13.3456f;
            Console.WriteLine(exFloat);

            //Decimal - Use This For Money!
            decimal exDecimal = 16.89m;
            Console.WriteLine(exDecimal);

            //Unsigned - Only Positive Values
            byte exByte = 214;
            Console.WriteLine(exByte);

            //UShort
            ushort exUshort = 45023;
            Console.WriteLine(exUshort);

            //UInt
            uint exUint = 4000000000;
            Console.WriteLine(exUint);

            //ULong
            ulong exUlong = 56000000000;
            Console.WriteLine(exUlong);
        }
    }
}
